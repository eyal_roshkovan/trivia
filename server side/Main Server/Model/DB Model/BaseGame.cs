﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main_Server.Model.DB_Model
{
    public class BaseGame : BaseEntity
    {
        public BaseUser Admin { get; protected set; }
        public string Category { get; protected set; }
        public BaseUser Winner { get; protected set; }
        public int QuestionsAmount { get; protected set; }

        public BaseGame(int id, BaseUser admin, string category, BaseUser winner, int questionsAmount): base(id)
        {
            Admin = admin;
            Category = category;
            Winner = winner;
            QuestionsAmount = questionsAmount;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
