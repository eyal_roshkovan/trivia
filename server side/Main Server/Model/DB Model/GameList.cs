﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main_Server.Model.DB_Model
{
    public class GameList : List<BaseGame>
    {
        public GameList() { }
        public GameList(IEnumerable<BaseGame> list) : base(list) { } //base here is List class

        public GameList(IEnumerable<BaseEntity> list) :
            base(list.Cast<BaseGame>().ToList())
        { }
    }

}
