﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main_Server.Model.DB_Model
{
    public class UserList : List<BaseUser>
    {
        public UserList() { }
        public UserList(IEnumerable<BaseUser> list) : base(list) { } //base here is List class

        public UserList(IEnumerable<BaseEntity> list) :
            base(list.Cast<BaseUser>().ToList())
        { }
    }
}
