﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main_Server.Model.DB_Model
{
    public class QuestionList : List<TriviaQuestion>
    {
        public QuestionList() { }
        public QuestionList(IEnumerable<TriviaQuestion> list) : base(list) { } //base here is List class

        public QuestionList(IEnumerable<BaseEntity> list) :
            base(list.Cast<TriviaQuestion>().ToList())
        { }
    }
}
