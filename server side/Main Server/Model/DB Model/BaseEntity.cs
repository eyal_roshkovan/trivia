﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main_Server.Model
{
    abstract public class BaseEntity
    {
        protected int id;
        public int Id
        {
            get; set;
        }

        public BaseEntity(int id)
        {
            Id = id;
        }

        public BaseEntity() { }
    }
}
