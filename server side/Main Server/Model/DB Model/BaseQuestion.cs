﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main_Server.Model.DB_Model
{
    public class TriviaQuestion : BaseEntity
    {
        public string QuestionString { get; set; }
        public string WrongAnswer1 { get; set; }
        public string WrongAnswer2 { get; set; }
        public string WrongAnswer3 { get; set; }
        public string CorrectAnswer { get; set; }
        public string Category { get; set; }

        public TriviaQuestion(int id, string question, string wrongAnswer1, string wrongAnswer2, string wrongAnswer3, string correctAnswer, string category) : base(id)
        {
            QuestionString = question;
            WrongAnswer1 = wrongAnswer1;
            WrongAnswer2 = wrongAnswer2;
            WrongAnswer3 = wrongAnswer3;
            CorrectAnswer = correctAnswer;
            Category = category;
        }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

}
