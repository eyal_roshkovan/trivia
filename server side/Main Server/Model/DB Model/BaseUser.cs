﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main_Server.Model.DB_Model
{
    public class BaseUser : BaseEntity
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public int TotalGamesPlayed { get; set; }
        public int TotalQuestionsAnswered { get; set; }

        public BaseUser(int id, string username, string password, string email, int totalGamesPlayed, int totalQuestionsAnswered) : base(id)
        {
            Username = username;
            Password = password;
            Email = email;
            TotalGamesPlayed = totalGamesPlayed;
            TotalQuestionsAnswered = totalQuestionsAnswered;
        }
        public BaseUser() : base()
        {

        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
