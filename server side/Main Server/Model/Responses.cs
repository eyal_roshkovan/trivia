﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;
public abstract class Response
{
    public abstract List<byte> SerializeResponse();
}

public class UserGamesWonResponse : Response
{
    public int GamesWon;
    public override List<byte> SerializeResponse()
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data["gamesWon"] = GamesWon;

        return JsonResponsePacketSerializer.ConvertIntoShape(data, (int)TypesOfRequest.USER_POINTS_CODE);
    }
}

public class WebAdminRefreshResponse : Response
{
    public GetPlayersInRoomResponse PlayersInRoom;
    public GetWaitingToApproveListResponse usersWaiting;

    public override List<byte> SerializeResponse()
    {
        string strData = "users waiting:\n";
        foreach (var userEntry in usersWaiting.Users)
        {
            strData += userEntry.GetUsername() + ", Member, " + userEntry.GetAmountOfGamesPlayed() + "\n";
        }
        strData += "users in room:\n";
        foreach (var playerEntry in PlayersInRoom.Users)
        {
            var user = playerEntry.Key;
            var additionalInfo = playerEntry.Value;
            strData += user.GetUsername() + ", " + additionalInfo + ", " + user.GetAmountOfGamesPlayed() + ", " + user.GetIsReady() + ", ";
        }
        Dictionary<string, object> data = new Dictionary<string, object>
        {
            { "data", strData},
        };
        return JsonResponsePacketSerializer.ConvertIntoShape(data, (int)TypesOfRequest.REFRESH_WEB_CODE);
    }
}

public class WebMemberRefreshResponse : Response
{
    public GetPlayersInRoomResponse PlayersInRoom;
    public RefreshResponse refreshResponse;

    public override List<byte> SerializeResponse()
    {
        string strData = refreshResponse.Status;
        foreach (var playerEntry in PlayersInRoom.Users)
        {
            var user = playerEntry.Key;
            var additionalInfo = playerEntry.Value;
            strData += user.GetUsername() + ", " + additionalInfo + ", " + user.GetAmountOfGamesPlayed() + ", " + user.GetIsReady() + "\n";
        }
        Dictionary<string, object> data = new Dictionary<string, object>
        {
            { "data", strData},
        };
        return JsonResponsePacketSerializer.ConvertIntoShape(data, (int)TypesOfRequest.REFRESH_WEB_CODE);
    }

}

public class LoginResponse : Response
{
    public states Status;
    public override List<byte> SerializeResponse()
    {
        Dictionary<string, object> data = new Dictionary<string, object>
        {
            { "status", Status }
        };

        return JsonResponsePacketSerializer.ConvertIntoShape(data, (int)TypesOfRequest.LOGIN_CODE);

    }

}

public class SignupResponse : Response
{
    public states Status;
    public override List<byte> SerializeResponse()
    {
        Dictionary<string, object> data = new Dictionary<string, object>
        {
            { "status", Status }
        };

        return JsonResponsePacketSerializer.ConvertIntoShape(data, (int)TypesOfRequest.SIGNUP_CODE);
    }

}

public class ErrorResponse : Response
{
    public string Message;
    public override List<byte> SerializeResponse()
    {
        Dictionary<string, object> data = new Dictionary<string, object>
        {
            { "message", Message }
        };

        return JsonResponsePacketSerializer.ConvertIntoShape(data, (int)TypesOfRequest.ERROR_CODE);

    }

}

public class GetTopWinnersResponse : Response
{
    public int Status;
    public List<Tuple<string, int, int>> Winners;
    public override List<byte> SerializeResponse()
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data["status"] = Status;
        data["Winners"] = Winners;

        return JsonResponsePacketSerializer.ConvertIntoShape(data, (int)TypesOfRequest.GET_TOP_WINNERS_CODE);
    }

};

public class GetTopCategoriesResponse : Response
{
    public List<Tuple<string, int>> topCategories;
    public override List<byte> SerializeResponse()
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data["Categories"] = topCategories;

        return JsonResponsePacketSerializer.ConvertIntoShape(data, (int)TypesOfRequest.GET_TOP_WINNERS_CODE);
    }

}

public class CloseRoomResponse : Response
{
    public int Status;
    public override List<byte> SerializeResponse()
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data["status"] = Status;

        return JsonResponsePacketSerializer.ConvertIntoShape(data, (int)TypesOfRequest.CLOSE_ROOM_CODE);
    }
}

public class StartGameResponse : Response
{
    public int Status;
    public override List<byte> SerializeResponse()
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data["status"] = Status;

        return JsonResponsePacketSerializer.ConvertIntoShape(data, (int)TypesOfRequest.START_GAME_CODE);

    }

}

public class LeaveRoomResponse : Response
{
    public int Status;
    public override List<byte> SerializeResponse()
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data["status"] = Status;

        return JsonResponsePacketSerializer.ConvertIntoShape(data, (int)TypesOfRequest.LEAVE_ROOM_CODE);
    }
}

public class ReadyUnreadyResponse : Response
{
    public int Status;
    public override List<byte> SerializeResponse()
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data["status"] = Status;

        return JsonResponsePacketSerializer.ConvertIntoShape(data, (int)TypesOfRequest.READY_NOTREADY_CODE);
    }

}



public class GetRoomStateResponse : Response
{
    public int Status;
    public bool HasGameBegun;
    public Dictionary<LoggedUser, string> Players;
    public int QuestionCount;
    public int AnswerTimeout;
    public override List<byte> SerializeResponse()
    {
        Dictionary<string, object> data = new Dictionary<string, object>();
        List<string> playersDataList = new List<string>();

        foreach (var kvp in Players)
        {
            playersDataList.Add($"{kvp.Key.GetUsername()}, {kvp.Value}, {kvp.Key.GetAmountOfGamesPlayed()}");
        }

        data["status"] = Status;
        data["hasGameBegun"] = HasGameBegun;
        data["players"] = playersDataList;
        data["answerCount"] = QuestionCount;
        data["answerTimeOut"] = AnswerTimeout;

        return JsonResponsePacketSerializer.ConvertIntoShape(data, (int)TypesOfRequest.GET_ROOM_STATE_CODE);
    }
}

public class GetIsOwnerResponse : Response
{
    public bool IsOwner;
    public override List<byte> SerializeResponse()
    {
        Dictionary<string, object> data = new Dictionary<string, object>
        {
            { "isOwner", IsOwner},
        };
        return JsonResponsePacketSerializer.ConvertIntoShape(data, (int)TypesOfRequest.REFRESH_WEB_CODE);
    }
}

public class GetOwnerRoomResponse : Response
{
    public Room Room;
    public override List<byte> SerializeResponse()
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        RoomData roomData = Room.GetMetaData();
        string roomInfo = $"{roomData.Name}, {roomData.RoomOwner}, {roomData.Category}, {roomData.IsPublic}, {roomData.MaxPlayers}, {roomData.CurrentAmountOfPlayers}, {roomData.Id}, {roomData.NumOfQuestionsInGame}, {roomData.TimePerQuestion}";

        data["Room"] = roomInfo;

        return JsonResponsePacketSerializer.ConvertIntoShape(data, (int)TypesOfRequest.GET_OWNER_ROOM_CODE);
    }
}

public class RefreshResponse : Response
{
    public string Status;
    public override List<byte> SerializeResponse()
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data["status"] = Status;

        return JsonResponsePacketSerializer.ConvertIntoShape(data, (int)TypesOfRequest.REFRESH_CODE);
    }

}
public class GetPlayersInRoomResponse : Response
{
    public Dictionary<LoggedUser, string> Users;
    public override List<byte> SerializeResponse()
    {
        Dictionary<string, object> data = new Dictionary<string, object>();
        string playersList = "";

        foreach (var playerEntry in Users)
        {
            var user = playerEntry.Key;
            var additionalInfo = playerEntry.Value;
            playersList += user.GetUsername() + ", " + additionalInfo + ", " + user.GetAmountOfGamesPlayed() + ", " + user.GetIsReady() + "\n";
        }

        data["PlayersInRoom"] = playersList;

        return JsonResponsePacketSerializer.ConvertIntoShape(data, (int)TypesOfRequest.PLAYERS_IN_ROOM_CODE);
    }

}

public class HasApprovedResponse : Response
{
    public int ApprovedCode;
    public override List<byte> SerializeResponse()
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data["approved"] = ApprovedCode;

        return JsonResponsePacketSerializer.ConvertIntoShape(data, (int)TypesOfRequest.HAS_APPROVED_CODE);

    }

}

public class StopWaitingResponse : Response
{
    public bool Succeded;
    public override List<byte> SerializeResponse()
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data["succeeded"] = Succeded;

        return JsonResponsePacketSerializer.ConvertIntoShape(data, (int)TypesOfRequest.STOP_WAITING_CODE);
    }

}

public class GetWaitingToApproveListResponse : Response
{
    public List<LoggedUser> Users;
    public override List<byte> SerializeResponse()
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        string players = "";
        foreach (var userEntry in Users)
        {
            players += userEntry.GetUsername() + ", Member, " + userEntry.GetAmountOfGamesPlayed() + "\n";
        }

        if (!string.IsNullOrEmpty(players))
        {
            players = players.TrimEnd('\n');
        }

        data["WaitingPlayers"] = players;

        return JsonResponsePacketSerializer.ConvertIntoShape(data, (int)TypesOfRequest.PLAYERS_IN_ROOM_CODE);
    }

}

public class ApprovedUserResponse : Response
{
    public int Status;
    public override List<byte> SerializeResponse()
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data["status"] = Status;

        return JsonResponsePacketSerializer.ConvertIntoShape(data, (int)TypesOfRequest.APPROVE_USER_CODE);
    }

}

public class LogoutResponse : Response
{
    public int Status;
    public override List<byte> SerializeResponse()
    {
        Dictionary<string, object> data = new Dictionary<string, object>
        {
            { "status", Status }
        };

        return JsonResponsePacketSerializer.ConvertIntoShape(data, (int)TypesOfRequest.LOGOUT_CODE);
    }

}

public class GetRoomsResponse : Response
{
    public int Status;
    public List<RoomData> Rooms;
    public override List<byte> SerializeResponse()
    {
        Dictionary<string, object> data = new Dictionary<string, object>();
        string roomsList = "";

        foreach (RoomData roomData in Rooms)
        {

            roomsList += roomData.Name;
            roomsList += ", ";
            roomsList += roomData.RoomOwner;
            roomsList += ", ";
            roomsList += roomData.Category;
            roomsList += ", ";
            roomsList += roomData.IsPublic;
            roomsList += ", ";
            roomsList += roomData.MaxPlayers;
            roomsList += ", ";
            roomsList += roomData.CurrentAmountOfPlayers;
            roomsList += ", ";
            roomsList += roomData.Id;
            roomsList += ", ";
            roomsList += roomData.NumOfQuestionsInGame;
            roomsList += ", ";
            roomsList += roomData.TimePerQuestion;
            roomsList += "\n";
        }

        data["status"] = Status;
        data["Rooms"] = roomsList;

        return JsonResponsePacketSerializer.ConvertIntoShape(data, (int)TypesOfRequest.GET_ROOMS_CODE);
    }

}

public class GetPersonalStatsResponse : Response
{
    public List<string> Statistics;
    public override List<byte> SerializeResponse()
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data["Stats"] = Statistics;

        return JsonResponsePacketSerializer.ConvertIntoShape(data, (int)TypesOfRequest.PERSONAL_STATISTICS_CODE);
    }

}

public class JoinRoomResponse : Response
{
    public int Status;
    public override List<byte> SerializeResponse()
    {
        Dictionary<string, object> data = new Dictionary<string, object>
        {
            { "status", Status }
        };

        return JsonResponsePacketSerializer.ConvertIntoShape(data, (int)TypesOfRequest.JOIN_ROOM_CODE);
    }

}

public class CreateRoomResponse : Response
{
    public int Status;
    public override List<byte> SerializeResponse()
    {
        Dictionary<string, object> data = new Dictionary<string, object>
        {
            { "status", Status }
        };

        return JsonResponsePacketSerializer.ConvertIntoShape(data, (int)TypesOfRequest.CREATE_ROOM_CODE);
    }

}

public class KickPlayerResponse : Response
{
    public bool Success;
    public override List<byte> SerializeResponse()
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data["succeeded"] = Success;

        return JsonResponsePacketSerializer.ConvertIntoShape(data, (int)TypesOfRequest.KICK_PLAYER_CODE);
    }

}

public class GetQuestionResponse : Response
{
    public int Status;
    public string Question;
    public Dictionary<int, string> Answers;
    public override List<byte> SerializeResponse()
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data["status"] = Status;
        data["question"] = Question;
        data["answers"] = Answers;

        return JsonResponsePacketSerializer.ConvertIntoShape(data, (int)TypesOfRequest.GET_QUESTION_CODE);
    }

}

public class SubmitAnswerResponse : Response
{
    public int Status;
    public int CorrectAnswerId;
    public override List<byte> SerializeResponse()
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data["status"] = Status;

        return JsonResponsePacketSerializer.ConvertIntoShape(data, (int)TypesOfRequest.SUBMIT_ANSWER_CODE);
    }
}

public class GetGameResultsResponse : Response
{
    public int Status;
    public List<PlayerResults> Results;
    public override List<byte> SerializeResponse()
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data["status"] = Status;

        // Convert results vector to JSON list
        string playersResults = "";
        foreach (PlayerResults playerResults in Results)
        {
            playersResults += playerResults.Username + ", " +
                playerResults.CorrectAnswerCount + ", " +
                playerResults.WrongAnswerCount + ", " +
                playerResults.AverageAnswerTime + "  ";
        }

        if (!string.IsNullOrEmpty(playersResults))
        {
            playersResults = playersResults.TrimEnd('\n');
        }

        data["results"] = playersResults;

        return JsonResponsePacketSerializer.ConvertIntoShape(data, (int)TypesOfRequest.GET_GAME_RESULTS_CODE);
    }

}

public class LeaveGameResponse : Response
{
    public int Status;
    public override List<byte> SerializeResponse()
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data["status"] = Status;

        return JsonResponsePacketSerializer.ConvertIntoShape(data, (int)TypesOfRequest.LEAVE_GAME_CODE);
    }
}

public class HasEveryoneFinishedResponse : Response
{
    public bool HasEveryoneFinished;
    public override List<byte> SerializeResponse()
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data["hasEveryoneFinished"] = HasEveryoneFinished;

        return JsonResponsePacketSerializer.ConvertIntoShape(data, (int)TypesOfRequest.HAS_EVERYONE_FINISHED_CODE);
    }

}
