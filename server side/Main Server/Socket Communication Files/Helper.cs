﻿using System;
using System.Net.Sockets;
using System.Text;
using System.Collections.Generic;

public class Helper
{
    public const int CODE_SIZE = 8;
    public const int DATA_LEN_SIZE = 32;


    public static int GetMessageCode(List<byte> buffer)
    {
        string code = "";
        for (int i = 0; i < 8; i++)
        {
            code += buffer[i];
        }
        return Convert.ToInt32(code, 2);
    }

    public static string GetDataFromSocket(Socket socket, int bytesNum)
    {
        int receivedBytes = 0;
        if (bytesNum == 0)
            return "";

        byte[] data = new byte[bytesNum];
        try
        {
            receivedBytes = socket.Receive(data, bytesNum, SocketFlags.None);
        }
        catch(Exception e)
        {
            Console.WriteLine(e);
        }
        if (receivedBytes == 0)
        {
            throw new Exception($"Error while receiving from socket: {socket}");
        }

        return Encoding.UTF8.GetString(data, 0, receivedBytes);
    }

    public static void SendData(Socket socket, byte[] data)
    {
        try
        {
            socket.Send(data);
        }
        catch (SocketException)
        {
            throw new Exception("Error while sending message to client");
        }
    }
}
