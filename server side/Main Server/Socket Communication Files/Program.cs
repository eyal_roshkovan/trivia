﻿using Main_Server.VIEW_DB;
using System;

namespace Main_Server
{
    internal class Program
    {
        static void Main()
        {
            BaseUserDB db = new BaseUserDB();
            Console.WriteLine(db.GetAmountOfGamesPlayed("Eyal1234"));

            Server server = new Server();
            server.Run();
            
            Console.ReadLine();
        }
    }
}