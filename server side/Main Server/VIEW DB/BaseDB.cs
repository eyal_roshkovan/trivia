﻿using Main_Server.Model;
using Microsoft.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using SQLitePCL;
using System.IO;
using System.Reflection;

namespace Main_Server.VIEW_DB
{
    public abstract class BaseDB
    {
        private static string filePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\Trivia.sqlite";

        private string connectionString = $@"Data Source={filePath};Version=3;";
        protected SQLiteConnection connection;
        protected SQLiteCommand command;
        protected SQLiteDataReader reader;

        protected abstract BaseEntity NewEntity(SQLiteDataReader reader);

        public BaseDB()
        {
            connection = new SQLiteConnection(connectionString);
            command = new SQLiteCommand();
            command.Connection = connection;
        }

        protected virtual BaseEntity CreateModel(BaseEntity entity)
        {
            if (entity != null)
            {
                entity.Id = Convert.ToInt32(reader["id"]);
            }
            return entity;
        }

        protected virtual List<BaseEntity> Select(string query)
        {
            List<BaseEntity> list = new List<BaseEntity>();
            string cs = $@"URI=file:{filePath}";
            var con = new SQLiteConnection(cs);
            con.Open();

            var cmd = new SQLiteCommand(query, con);
            SQLiteDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                list.Add(NewEntity(rdr));
            }
            return list;
        }

        protected int SaveChanges(string command_text)
        {
            int records = 0;
            try
            {
                command.CommandText = command_text;
                connection.Open();
                records = command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message + "\nSQL:" + command.CommandText);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }
            return records;
        }
    }
}
