﻿using Main_Server.Model;
using Main_Server.Model.DB_Model;
using Microsoft.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Main_Server.VIEW_DB
{
    public class BaseUserDB : BaseDB
    {
        protected override BaseEntity CreateModel(BaseEntity entity)
        {
            entity = base.CreateModel(entity);
            BaseUser user = entity as BaseUser;
            if (user != null) //האם הצלחתי בהמרה, האם הטיפוס מתאים?
            {
                user.Username = reader["username"].ToString();
                user.Password = reader["password"].ToString();
                user.Email = reader["email"].ToString();
                user.TotalQuestionsAnswered = int.Parse(reader["totalQuestionsAnswered"].ToString());
                user.Id = int.Parse(reader["id"].ToString());
                user.TotalGamesPlayed = int.Parse(reader["totalGamesPlayed"].ToString());
            }
            return user;
        }

        protected override BaseEntity NewEntity(SQLiteDataReader reader)
        {
            return new BaseUser(int.Parse(reader.GetValue(0).ToString()), reader.GetValue(1).ToString(), reader.GetValue(2).ToString(), reader.GetValue(3).ToString(), int.Parse(reader.GetValue(4).ToString()), int.Parse(reader.GetValue(5).ToString()));
        }

        private static bool IsValidUsername(string username)
        {
            return username.Length > 3;
        }
        private static bool IsValidPassword(string password)
        {
            if (password.Length < 8)
            {
                return false;
            }

            // Check for at least one capital letter
            if (!Regex.IsMatch(password, "[A-Z]"))
            {
                return false;
            }

            // Check for at least one digit
            if (!Regex.IsMatch(password, "[0-9]"))
            {
                return false;
            }

            return true;
        }

        private static bool IsValidEmail(string email)
        {
            // Check for a valid email pattern
            string pattern = @"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$";
            Regex regex = new Regex(pattern);

            return regex.IsMatch(email);
        }

        public RequestResult Signup(string username, string password, string email)
        {
            RequestResult result = new RequestResult();
            Response response;
            // Check if the username already exists
            if (DoesUsernameExist(username))
            {
                Console.WriteLine("Username already exists");
                response = new ErrorResponse();
                ((ErrorResponse)response).Message = "Username already exists";
            }
            else if (DoesEmailExist(username))
            {
                Console.WriteLine("Email already exists");
                response = new ErrorResponse();
                ((ErrorResponse)response).Message = "Email already exists";
            }
            else if (!IsValidPassword(password))
            {
                Console.WriteLine("Not a valid password");
                response = new ErrorResponse();
                ((ErrorResponse)response).Message = "Not a valid password";
            }
            else if (!IsValidUsername(username))
            {
                Console.WriteLine("Not a valid username");
                response = new ErrorResponse();
                ((ErrorResponse)response).Message = "Not a valid username";
            }
            else if (!IsValidEmail(email))
            {
                Console.WriteLine("Not a valid email");
                response = new ErrorResponse();
                ((ErrorResponse)response).Message = "Not a valid email";
            }
            else
            {
                // Insert the new user into the database
                string query = $"INSERT INTO Users (Username, Password, Email) VALUES ('{username}', '{password}', '{email}')";
                response = new LoginResponse();

                if(SaveChanges(query) == 1) // if cool
                {
                    ((LoginResponse)response).Status = states.success;
                    LoggedUser user = new LoggedUser(username, GetAmountOfGamesPlayed(username));
                    result.NewHandler = new MenuRequestHandler(user);
                }
                else
                {
                    ((LoginResponse)response).Status = states.fail;
                }
                result.Buffer = response.SerializeResponse();
            }
            
            return result;
        }
        private int GetAmountOfGamesPlayed(string username)
        {
            string query = $"SELECT * FROM Users WHERE Username = '{username}'";
            UserList users = new UserList(Select(query));
            return users[0].TotalGamesPlayed;
        }

        private bool DoesUsernameExist(string username)
        {
            string query = $"SELECT * FROM Users WHERE Username = '{username}'";
            UserList users = new UserList(Select(query));
            return users.Count > 0;
        }

        private bool DoesEmailExist(string email)
        {
            string query = $"SELECT * FROM Users WHERE email = '{email}'";
            UserList users = new UserList(Select(query));
            return users.Count > 0;
        }

        public int TryLogin(string  username, string password)
        {
            if(!DoesUsernameExist(username))
            {
                return -1;
            }
            string query = $"SELECT * FROM Users WHERE Username = '{username}' AND Password = '{password}'";
            UserList users = new UserList(Select(query));
            return users.Count;
        }
    }
}
