﻿using Microsoft.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;

public class DBConnector
{
    private static OleDbConnection db;

    private readonly static string dbFileName = "C:\\Users\\User\\Desktop\\projects\\trivia-project\\Sockets Project\\server side\\Main Server\\Database.mdb";

    public static bool Open()
    {
        try
        {
            // Use the Microsoft.Jet.OLEDB.4.0 provider for SQLite
            db = new OleDbConnection($"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={dbFileName};");

            db.Open();

            if (db.State != ConnectionState.Open)
            {
                Console.WriteLine("Error with opening the database");
                return false;
            }

            return true;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
            return false;
        }
    }

    public static bool Close()
    {
        try
        {
            db.Close();
            db = null;
        }
        catch
        {
            return false;
        }
        return true;
    }

    public static int DoesPasswordMatch(string userName, string password)
    {
        string sqlStatement = $"SELECT * FROM USERS WHERE UserName = '{userName}' AND Pass = '{password}'";
        int exist = 0;
        if (!ExecuteReader(sqlStatement, (reader) => exist = 1))
            return 0;

        return exist;
    }

    public static int AddNewUser(string userName, string password, string email)
    {
        string sqlStatement = $"INSERT INTO USERS (UserName, Pass, Email, GamesPlayed, QuestionsAnswered) VALUES ('{userName}', '{password}', '{email}', 0, 0)";

        if (!ExecuteNonQuery(sqlStatement))
            return 0;

        return 1;
    }

    public static int GetAmountOfGamesWon(string username)
    {
        int numOfWins = 0;
        string sqlStatement = $"SELECT COUNT(*) AS GamesWon\r\nFROM GameStatistics\r\nWHERE WinnerUserName = {username};\r\n";
        if (!ExecuteReader(sqlStatement, (reader) => numOfWins = reader.GetInt32(0)))
            return 0;

        return numOfWins;
    }

    public static List<Tuple<string, int, int>> GetTopWinnersWithGamesPlayed()
    {
        List<Tuple<string, int, int>> topWinnersWithGamesPlayed = new List<Tuple<string, int, int>>();
        int gamesPlayed = 0;
        // string sqlStatement = "SELECT TOP 5 WinnerUserName, COUNT(*) AS GamesWon\r\nFROM GameStatistics\r\nGROUP BY WinnerUserName\r\nORDER BY COUNT(*) DESC;\r\n";
        string sqlStatement = "SELECT TOP 5\r\n    WinnerUserName,\r\n    COUNT(*) AS GamesWon\r\nFROM\r\n    GameStatistics\r\nGROUP BY\r\n    WinnerUserName\r\nORDER BY\r\n    COUNT(*) DESC;\r\n";
        ExecuteReader(sqlStatement, (reader) =>
        {
            Console.WriteLine(reader.GetString(0));
            Console.WriteLine(reader.GetString(1));
        });

        return topWinnersWithGamesPlayed;
    }

    public static int GetNumOfAnswers(string userName)
    {
        int numOfCorrectAnswer = 0;

        string sqlStatement = $"SELECT QuestionsAnswered FROM USERS WHERE UserName = '{userName}'";
        if (!ExecuteReader(sqlStatement, (reader) => numOfCorrectAnswer = reader.GetInt32(0)))
            return 0;

        return numOfCorrectAnswer;
    }

    public static List<Tuple<string, int>> GetTopSelectedCategories()
    {
        List<Tuple<string, int>> topSelectedCategories = new List<Tuple<string, int>>();


        string sqlStatement = "SELECT TOP 3 GameCategory, COUNT(*) AS GamesPlayed " +
                      "FROM GameStatistics " +
                      "GROUP BY GameCategory " +
                      "ORDER BY COUNT(*) DESC";



        ExecuteReader(sqlStatement, (reader) =>
        {
            string category = reader.GetString(0);
            int gamesPlayed = reader.GetInt32(1);
            topSelectedCategories.Add(new Tuple<string, int>(category, gamesPlayed));
        });

        return topSelectedCategories;
    }

    public static int GetNumOfPlayerGames(string username)
    {
        int numOfGames = 0;

        string sqlStatement = $"SELECT GamesPlayed FROM USERS WHERE UserName = '{username}'";
        if (!ExecuteReader(sqlStatement, (reader) => numOfGames = reader.GetInt32(0)))
            return 0;

        return numOfGames;
    }

    public static int GetRoomsCreatedCount(string username)
    {
        int roomsCreatedCount = 0;

        string sqlStatement = $"SELECT COUNT(*) AS RoomsCreated " +
                              $"FROM GameStatistics " +
                              $"WHERE AdminUserName = '{username}'";

        ExecuteReader(sqlStatement, (reader) =>
        {
            roomsCreatedCount = reader.GetInt32(0);
        });

        return roomsCreatedCount;
    }

    public static List<string> GetPersonalStat(string userName)
    {
        List<string> userStat = new List<string>();

        userStat.Add(GetGamesWonByUser(userName).ToString()); // Amount of won games
        userStat.Add(GetRoomsCreatedCount(userName).ToString()); // Rooms created
        userStat.Add(GetNumOfAnswers(userName).ToString()); // Num of Total Answers
        userStat.Add(GetNumOfPlayerGames(userName).ToString()); // Num of Games that played

        return userStat;
    }

    public static List<Question> GetQuestionsByCategoryAndNumber(string category, int limit)
    {
        List<Question> questions = new List<Question>();

        string sqlStatement = $"SELECT TOP {limit} Question, FirstWrongAnswer, SecondWrongAnswer, ThirdWrongAnswer, RightAnswer FROM QuestionsAnswers WHERE Category = '{category}' ORDER BY RND(-Timer())";

        if (!ExecuteReader(sqlStatement, (reader) =>
        {
            string question = reader.GetString(0);
            string firstWrongAnswer = reader.GetString(1);
            string secondWrongAnswer = reader.GetString(2);
            string thirdWrongAnswer = reader.GetString(3);
            string rightAnswer = reader.GetString(4);

            List<string> possibleAnswers = new List<string> { firstWrongAnswer, secondWrongAnswer, thirdWrongAnswer, rightAnswer };
            Random rng = new Random();
            possibleAnswers = possibleAnswers.OrderBy(item => rng.Next()).ToList();

            int correctAnswerId = possibleAnswers.FindIndex(a => a == rightAnswer) + 1;

            Question questionObj = new Question(question, possibleAnswers, correctAnswerId);
            questions.Add(questionObj);
        }))
        {
            // Handle error, such as logging or error reporting
        }

        return questions;
    }

    public static void IncrementNumberOfQuestionAnswered(string userName)
    {
        string sqlStatement = $"UPDATE USERS SET QuestionsAnswered = QuestionsAnswered + 1 WHERE UserName = '{userName}'";
        ExecuteNonQuery(sqlStatement);
    }

    public static void UserPlayGame(string username, int questionsAnsweredIncrement)
    {
        string sqlStatement = $"UPDATE USERS SET GamesPlayed = GamesPlayed + 1, " +
                             $"QuestionsAnswered = QuestionsAnswered + {questionsAnsweredIncrement} " +
                             $"WHERE UserName = '{username}'";

        ExecuteNonQuery(sqlStatement);
    }


    public static void IncrementCountOfGames(string userName)
    {
        string sqlStatement = $"UPDATE USERS SET GamesPlayed = GamesPlayed + 1 WHERE UserName = '{userName}'";
        ExecuteNonQuery(sqlStatement);
    }

    public static int GetGamesWonByUser(string username)
    {
        int gamesWon = 0;

        string sqlStatement = $"SELECT COUNT(*) AS GamesWon " +
                              $"FROM GameStatistics " +
                              $"WHERE WinnerUserName = '{username}'";

        ExecuteReader(sqlStatement, (reader) =>
        {
            gamesWon = reader.GetInt32(0);
        });

        return gamesWon;
    }

    public static void UpdateGameStatistics(Game game)
    {
        LoggedUser winner = game.GetWinner();
        string name = "NULL";
        if (winner != null) 
        {
            name = "'" + winner.GetUsername() + "'";
        }
        string updateStatement = $"INSERT INTO GameStatistics (AdminUserName, GameCategory, QuestionsAnswered, WinnerUserName) " +
                                 $"VALUES ('{game.GetRoom().GetOwner().GetUsername()}', '{game.GetRoom().GetMetaData().Category}', {game.GetRoom().GetMetaData().NumOfQuestionsInGame}, {name})";

        ExecuteNonQuery(updateStatement);

        foreach(LoggedUser user in game.GetRoom().GetAllUsers().Keys)
        {
            UserPlayGame(user.GetUsername(), game.GetRoom().GetMetaData().NumOfQuestionsInGame);
        }
    }

    private static bool ExecuteNonQuery(string sqlStatement)
    {
        try
        {
            using (OleDbCommand command = new OleDbCommand(sqlStatement, db))
            {
                command.ExecuteNonQuery();
                return true;
            }
        }
        catch
        {
            return false;
        }
    }

    private static bool ExecuteReader(string sqlStatement, Action<OleDbDataReader> action)
    {
        try
        {
            using (OleDbCommand command = new OleDbCommand(sqlStatement, db))
            using (OleDbDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    action(reader);
                }
            }
            return true;
        }
        catch
        {
            return false;
        }
    }

}

