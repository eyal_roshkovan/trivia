﻿using System;

public class LoggedUser
{
    private string username;
    private bool isReady;
    private Room ownedRoom;
    private int amountOfGamesPlayed;
    private RoomAdminRequestHandler roomWaitingToApprove;

    public LoggedUser(string userName, int amountOfGamesPlayed)
    {
        username = userName;
        isReady = false;
        ownedRoom = null;
        roomWaitingToApprove = null;
        this.amountOfGamesPlayed = amountOfGamesPlayed;
    }

    public string GetUsername()
    {
        return username;
    }

    public int GetAmountOfGamesPlayed()
    {
        return amountOfGamesPlayed;
    }
    public void PlayGame()
    {
        amountOfGamesPlayed++;
    }
    
    public void ReadyUnready()
    {
        isReady = !isReady;
    }

    public bool GetIsReady()
    {
        return isReady;
    }

    public void AddRoom(Room room)
    {
        ownedRoom = room;
    }

    public void DeleteOwnedRoom()
    {
        ownedRoom = null;
    }

    public void ClearRoomList()
    {
        if (roomWaitingToApprove != null)
        {
            roomWaitingToApprove.RemoveUser(username);
            roomWaitingToApprove = null;
        }
    }

    public void AddRoom(RoomAdminRequestHandler room)
    {
        roomWaitingToApprove = room;
    }

    public bool IsApproved()
    {
        return roomWaitingToApprove.HasUserBeenApproved(this) != 0;
    }

    public Room GetWaitingRoom()
    {
        return roomWaitingToApprove?.GetRoom();
    }   

    public bool Equals(LoggedUser other)
    {
        return username == other.username;
    }
}

public static class LoggedUserExtensions
{
    public static bool Equals(this LoggedUser lhs, LoggedUser rhs)
    {
        return lhs.GetUsername() == rhs.GetUsername();
    }

    public static bool LessThan(this LoggedUser lhs, LoggedUser rhs)
    {
        return string.Compare(lhs.GetUsername(), rhs.GetUsername(), StringComparison.Ordinal) < 0;
    }
}
