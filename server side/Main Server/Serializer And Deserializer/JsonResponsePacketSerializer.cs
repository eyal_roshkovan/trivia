﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Runtime.InteropServices.ComTypes;

public static class JsonResponsePacketSerializer
{
    public static List<byte> ConvertIntoShape(Dictionary<string, object> data, int code)
    {
        List<byte> buffer = new List<byte>();
        string strBuffer;
        string strJson;

        strJson = Newtonsoft.Json.JsonConvert.SerializeObject(data);

        // Insert code 
        strBuffer = Convert.ToString(code, 2).PadLeft(Helper.CODE_SIZE, '0');

        // Insert len of data
        int jLen = strJson.Length;
        strBuffer += Convert.ToString(jLen, 2).PadLeft(Helper.DATA_LEN_SIZE, '0');

        // Insert data
        foreach (char ch in strJson)
            strBuffer += Convert.ToString(ch, 2).PadLeft(8, '0');

        foreach (char ch in strBuffer)
        {
            if (ch == '0')
                buffer.Add(0);
            else
                buffer.Add(1);
        }

        return buffer;
    }
}
