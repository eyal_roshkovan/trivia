﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Linq;

public static class JsonRequestPacketDeserializer
{
    private static string GetString(List<byte> buffer)
    {
        string strLen = "";
        for (int i = Helper.CODE_SIZE; i < Helper.DATA_LEN_SIZE + Helper.CODE_SIZE; i++)
        {
            strLen += buffer[i];
        }

        int dataLen = Convert.ToInt32(strLen, 2);

        string binaryStr = "";
        buffer.RemoveRange(0, Helper.DATA_LEN_SIZE + Helper.CODE_SIZE);
        foreach (char ch in buffer)
        {
            binaryStr += ch;
        }

        string bytes = "";
        for (int i = 0; i < dataLen; i += 8)
        {
            string bits = "";
            for(int j = 0; j < 8;j++)
            {
                if (binaryStr[i + j] == 0)
                    bits += '0';
                else
                    bits += '1';

            }
            bytes += (char)Convert.ToByte(bits, 2);
        }
        return bytes;
    }
    public static LoginRequest DeserializeLoginRequest(List<byte> buffer)
    {
        LoginRequest loginRequest = new LoginRequest();

        string password = "";
        string userName = "";

        // Decode bytes from binary to regular string
        string jsonString = GetString(buffer);
        try
        {
            JObject jObject = JObject.Parse(jsonString); // convert json string
            if (jObject != null)
            {
                // extract values from json
                foreach (KeyValuePair<string, JToken> kvp in jObject)
                {
                    string key = kvp.Key;
                    JToken value = kvp.Value;

                    if (key == "username")
                        userName = value.ToString();

                    if (key == "password")
                        password = value.ToString();
                }
            }
        }
        catch (Exception)
        {
            // handle parse error
        }

        loginRequest.Password = password;
        loginRequest.Username = userName;
        return loginRequest;
    }
    public static SignupRequest DeserializeSignupRequest(List<byte> buffer)
    {
        SignupRequest request = new SignupRequest();

        string password = "";
        string userName = "";
        string email = "";

        // Decode bytes from binary to regular string
        string jsonString = GetString(buffer);
        try
        {
            JObject jObject = JObject.Parse(jsonString); // convert json string
            if (jObject != null)
            {
                // extract values from json
                foreach (KeyValuePair<string, JToken> kvp in jObject)
                {
                    string key = kvp.Key;
                    JToken value = kvp.Value;

                    if (key == "username")
                        userName = value.ToString() ?? "";
                    if (key == "password")
                        password = value.ToString() ?? "";
                    if (key == "email")
                        email = value.ToString() ?? "";
                }
            }
        }
        catch (Exception)
        {
            // handle parse error
        }
        request.Password = password;
        request.Username = userName;
        request.Email = email;

        return request;
    }

    public static GetPlayersInRoomRequest DeserializeGetPlayersRequest(List<byte> buffer)
    {
        GetPlayersInRoomRequest request = new GetPlayersInRoomRequest();

        int roomID = 0;

        // Decode bytes from binary to regular string
        string jsonString = GetString(buffer);
        try
        {
            JObject jObject = JObject.Parse(jsonString); // convert json string
            if (jObject != null)
            {
                // extract values from json
                foreach (KeyValuePair<string, JToken> kvp in jObject)
                {
                    string key = kvp.Key;
                    JToken value = kvp.Value;

                    if (key == "roomID")
                        roomID = value.ToObject<int>();
                }
            }
        }
        catch (Exception)
        {
            // handle parse error
        }
        request.RoomID = roomID;
        return request;
    }

    public static JoinRoomRequest DeserializeJoinRoomRequest(List<byte> buffer)
    {
        JoinRoomRequest request = new JoinRoomRequest();

        int roomID = 0;

        // Decode bytes from binary to regular string
        string jsonString = GetString(buffer);
        try
        {
            JObject jObject = JObject.Parse(jsonString); // convert json string
            if (jObject != null)
            {
                // extract values from json
                foreach (KeyValuePair<string, JToken> kvp in jObject)
                {
                    string key = kvp.Key;
                    JToken value = kvp.Value;

                    if (key == "roomID")
                        roomID = value.ToObject<int>();
                }
            }
        }
        catch (Exception)
        {
            // handle parse error
        }
        request.RoomID = roomID;
        return request;
    }

    public static CreateRoomRequest DeserializeCreateRoomRequest(List<byte> buffer)
    {
        CreateRoomRequest request = new CreateRoomRequest();

        int maxUsers = 0;
        int questionCount = 0;
        int answerTimeout = 0;
        string roomName = "";
        string category = "";
        string privacy = "";

        // Decode bytes from binary to regular string
        string jsonString = GetString(buffer);
        try
        {
            JObject jObject = JObject.Parse(jsonString); // convert json string
            if (jObject != null)
            {
                // extract values from json
                foreach (KeyValuePair<string, JToken> kvp in jObject)
                {
                    string key = kvp.Key;
                    JToken value = kvp.Value;

                    if (key == "maxUsers")
                        maxUsers = value.ToObject<int>();
                    else if (key == "questionCount")
                        questionCount = value.ToObject<int>();
                    else if (key == "answerTimeout")
                        answerTimeout = value.ToObject<int>();
                    else if (key == "roomName")
                        roomName = value.ToString() ?? "";
                    else if (key == "category")
                        category = value.ToString() ?? "";
                    else if (key == "privacy")
                        privacy = value.ToString() ?? "";
                }
            }
        }
        catch (Exception)
        {
            // handle parse error
        }

        request.AnswerTimeout = answerTimeout;
        request.MaxUsers = maxUsers;
        request.QuestionCount = questionCount;
        request.RoomName = roomName;
        request.Category = category;
        request.Privacy = privacy;
        return request;
    }
    public static ReadyUnreadyRequest DeserializeReadyUnreadyRequest(List<byte> buffer)
    {
        ReadyUnreadyRequest request = new ReadyUnreadyRequest();

        bool ready = false;

        // Decode bytes from binary to regular string
        string jsonString = GetString(buffer);
        try
        {
            JObject jObject = JObject.Parse(jsonString); // convert json string
            if (jObject != null)
            {
                // extract values from json
                foreach (KeyValuePair<string, JToken> kvp in jObject)
                {
                    string key = kvp.Key;
                    JToken value = kvp.Value;

                    if (key == "ready")
                        ready = value.ToObject<bool>();
                }
            }
        }
        catch (Exception)
        {
            // handle parse error
        }
        request.ReadyOrNot = ready;
        return request;
    }

    public static ApprovedUserRequest DeserializeApprovedUserRequest(List<byte> buffer)
    {
        ApprovedUserRequest request = new ApprovedUserRequest();

        string username = "";
        bool approved = false;

        // Decode bytes from binary to regular string
        string jsonString = GetString(buffer);
        try
        {
            JObject jObject = JObject.Parse(jsonString); // convert json string
            if (jObject != null)
            {
                // extract values from json
                foreach (KeyValuePair<string, JToken> kvp in jObject)
                {
                    string key = kvp.Key;
                    JToken value = kvp.Value;

                    if (key == "username")
                        username = value.ToString() ?? "";
                    if (key == "approved")
                        approved = value.ToObject<bool>();
                }
            }
        }
        catch (Exception)
        {
            // handle parse error
        }
        request.Username = username;
        request.Approved = approved;
        return request;
    }

    public static KickPlayerRequest DeserializeKickPlayerRequest(List<byte> buffer)
    {
        KickPlayerRequest request = new KickPlayerRequest();

        string username = "";

        // Decode bytes from binary to regular string
        string jsonString = GetString(buffer);
        try
        {
            JObject jObject = JObject.Parse(jsonString); // convert json string
            if (jObject != null)
            {
                // extract values from json
                foreach (KeyValuePair<string, JToken> kvp in jObject) 
                {
                    string key = kvp.Key;
                    JToken value = kvp.Value;

                    if (key == "username")
                        username = value.ToString() ?? "";
                }
            }
        }
        catch (Exception)
        {
            // handle parse error
        }
        request.Username = username;
        return request;
    }

    public static SubmitAnswerRequest DeserializeSubmitAnswerRequest(List<byte> buffer)
    {
        SubmitAnswerRequest request = new SubmitAnswerRequest();

        int answerId = 0;

        // Decode bytes from binary to regular string
        string decodedString = GetString(buffer);
        try
        {
            JObject jObject = JObject.Parse(decodedString); // convert json string
            if (jObject != null)
            {
                // extract values from json
                foreach (KeyValuePair<string, JToken> kvp in jObject)
                {
                    string key = kvp.Key;
                    JToken value = kvp.Value;

                    if (key == "answerId")
                    {
                        answerId = value.ToObject<int>();
                    }
                }
            }
        }
        catch (Exception)
        {
            // handle parse error
        }
        request.AnswerId = answerId;
        return request;
    }



}
