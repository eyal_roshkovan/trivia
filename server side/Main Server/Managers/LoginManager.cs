﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

public class LoginManager
{
    private List<LoggedUser> loggedUsers = new List<LoggedUser>();
    private object _loggedUserLock = new object();
    private static LoginManager instance;

    private LoginManager()
    {
        DBConnector.Open();
    }
    public static LoginManager GetInstance()
    {
        if (instance == null)
            instance = new LoginManager();
        return instance;
    }

    public bool Signup(string userName, string password, string email)
    {
        if(userName.Contains("users waiting") || userName.Contains("users in room"))
            return false;

        int res = DBConnector.AddNewUser(userName, password, email);
        if (res == 0)
            return false;

        LoggedUser newUser = new LoggedUser(userName, DBConnector.GetNumOfPlayerGames(userName));

        lock (_loggedUserLock)
        {
            loggedUsers.Add(newUser);
        }

        return true;
    }

    public bool Login(string username, string password)
    {
        int res = DBConnector.DoesPasswordMatch(username, password);
        if (res == 0)
            return false;

        lock (_loggedUserLock)
        {
            if (loggedUsers.Any(user => user.GetUsername() == username))
                return false;

            LoggedUser newUser = new LoggedUser(username, DBConnector.GetNumOfPlayerGames(username));
            loggedUsers.Add(newUser);
        }

        return true;
    }

    public void Logout(string userName)
    {
        lock (_loggedUserLock)
        {
            var user = loggedUsers.Find(u => u.GetUsername() == userName);
            if (user != null)
            {
                loggedUsers.Remove(user);
                Console.WriteLine($"User {userName} logged out.");
            }
        }
    }
}
