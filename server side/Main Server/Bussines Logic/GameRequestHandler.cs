﻿using System;
using System.Collections.Generic;

public class GameRequestHandler : IRequestHandler
{
    private readonly LoggedUser user;
    private readonly Game game;

    public GameRequestHandler(LoggedUser user, Room room)
    {
        this.user = user;
        game = GameManager.GetInstance().CreateGame(room);
        GameManager.GetInstance().IncGamesInTable(user);
    }

    public RequestResult HandleRequest(RequestInfo requestInfo)
    {
        try
        {
            RequestResult requestResult = new RequestResult();

            switch (requestInfo.CodeID)
            {
                case TypesOfRequest.ERROR_CODE:
                    HandleProgramClosed();
                    requestResult.NewHandler = new MenuRequestHandler(user);
                    break;
                case TypesOfRequest.GET_GAME_RESULTS_CODE:
                    requestResult = GetGameResults(requestInfo);
                    break;
                case TypesOfRequest.LEAVE_GAME_CODE:
                    requestResult = LeaveGame(requestInfo);
                    break;
                case TypesOfRequest.SUBMIT_ANSWER_CODE:
                    requestResult = SubmitAnswer(requestInfo);
                    break;
                case TypesOfRequest.GET_QUESTION_CODE:
                    requestResult = GetQuestion(requestInfo);
                    break;
                case TypesOfRequest.HAS_EVERYONE_FINISHED_CODE:
                    requestResult = HasEveryoneFinished(requestInfo);
                    break;
            }

            return requestResult;
        }
        catch(Exception e)
        {
            Console.WriteLine("Problem");
            throw e;
        }

    }

    public void HandleProgramClosed()
    {
        GameManager.GetInstance().RemovePlayer(user, game);
    }

    public RequestResult HasEveryoneFinished(RequestInfo requestInfo)
    {
        RequestResult requestResult = new RequestResult();
        HasEveryoneFinishedResponse response = new HasEveryoneFinishedResponse();

        response.HasEveryoneFinished = GameManager.GetInstance().HasEveryoneFinished(game);
        requestResult.Buffer = response.SerializeResponse();
        requestResult.NewHandler = this;

        return requestResult;
    }

    public RequestResult GetQuestion(RequestInfo requestInfo)
    {
        RequestResult requestResult = new RequestResult();

        Question question = GameManager.GetInstance().GetQuestionForUserAndGame(user, game);
        GetQuestionResponse response = new GetQuestionResponse();

        if (question != null)
        {
            response.Question = question.GetQuestion();

            // Convert vector to dictionary
            Dictionary<int, string> mapPossibleAnswers = new Dictionary<int, string>();
            int index = 1;
            List<string> vectorPossibleAnswers = question.GetPossibleAnswers();

            foreach (string str in vectorPossibleAnswers)
            {
                mapPossibleAnswers[index++] = str;
            }

            response.Answers = mapPossibleAnswers;
            response.Status = 1;
        }
        else
        {
            response.Status = 0;
            response.Question = "";
            response.Answers = new Dictionary<int, string>();
        }

        requestResult.Buffer = response.SerializeResponse();
        requestResult.NewHandler = this;

        return requestResult;
    }

    public RequestResult SubmitAnswer(RequestInfo requestInfo)
    {
        RequestResult requestResult = new RequestResult();

        SubmitAnswerResponse response = new SubmitAnswerResponse(); 
        SubmitAnswerRequest request = JsonRequestPacketDeserializer.DeserializeSubmitAnswerRequest(requestInfo.Buffer);

        response.CorrectAnswerId = GameManager.GetInstance().SubmitAnswer(user, game, request.AnswerId);

        requestResult.Buffer = response.SerializeResponse();
        requestResult.NewHandler = this;

        return requestResult;
    }

    public RequestResult GetGameResults(RequestInfo requestInfo)
    {
        RequestResult requestResult = new RequestResult();

        GetGameResultsResponse response = new GetGameResultsResponse();
        response.Status = (int)states.success;
        response.Results = GameManager.GetInstance().GetGameResultsOfGame(game, response.Status);

        requestResult.Buffer = response.SerializeResponse();
        requestResult.NewHandler = this;

        return requestResult;
    }

    public RequestResult LeaveGame(RequestInfo requestInfo)
    {
        RequestResult requestResult = new RequestResult();

        LeaveGameResponse response = new LeaveGameResponse();
        GameManager.GetInstance().RemovePlayer(user, game);
        response.Status = 1;

        requestResult.Buffer = response.SerializeResponse();
        requestResult.NewHandler = new MenuRequestHandler(user);

        return requestResult;
    }
}
