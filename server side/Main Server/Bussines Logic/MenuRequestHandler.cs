﻿using System;
using System.Collections.Generic;
using System.Linq;

public class MenuRequestHandler : IRequestHandler
{
    private readonly LoggedUser user;
    private readonly List<Room> notApprovedRooms = new List<Room>();

    public MenuRequestHandler(LoggedUser user)
    {
        this.user = user;
    }

    public RequestResult HandleRequest(RequestInfo requestInfo)
    {
        RequestResult requestResult = new RequestResult();
        requestResult.NewHandler = this;

        switch (requestInfo.CodeID)
        {
            case TypesOfRequest.ERROR_CODE:
                HandleProgramClosed();
                break;

            case TypesOfRequest.LOGOUT_CODE:
                requestResult = Signout();
                break;

            case TypesOfRequest.JOIN_ROOM_CODE:
                requestResult = JoinRoom(requestInfo);
                break;

            case TypesOfRequest.CREATE_ROOM_CODE:
                requestResult = CreateRoom(requestInfo);
                break;

            case TypesOfRequest.GET_ROOMS_CODE:
                requestResult = GetRooms();
                break;

            case TypesOfRequest.GET_TOP_WINNERS_CODE:
                requestResult = GetTopWinners();
                break;

            case TypesOfRequest.GET_TOP_CATEGORIES_CODE:
                requestResult = GetTopCategories();
                break;

            case TypesOfRequest.PERSONAL_STATISTICS_CODE:
                requestResult = GetPersonalStats();
                break;

            case TypesOfRequest.HAS_APPROVED_CODE:
                requestResult = HasApproved();
                break;

            case TypesOfRequest.STOP_WAITING_CODE:
                requestResult = StopWaiting();
                break;

            default:
                break;
        }

        return requestResult;
    }

    public void HandleProgramClosed()
    {
        if (user.GetUsername() != null)
        {
            StopWaiting();
        }

        Signout();
    }

    public RequestResult GetRooms()
    {
        RequestResult requestResult = new RequestResult();

        GetRoomsResponse rooms = new GetRoomsResponse
        {
            Status = 1,
            Rooms = RoomManager.GetInstance().GetRooms()
        };

        requestResult.Buffer = rooms.SerializeResponse();
        requestResult.NewHandler = this;

        return requestResult;
    }

    public RequestResult GetPlayersInRoom(RequestInfo requestInfo)
    {
        RequestResult requestResult = new RequestResult();

        GetPlayersInRoomRequest request = JsonRequestPacketDeserializer.DeserializeGetPlayersRequest(requestInfo.Buffer);

        GetPlayersInRoomResponse playersResponse = new GetPlayersInRoomResponse
        {
            Users = RoomManager.GetInstance().GetRoom(request.RoomID)?.GetAllUsers()
        };

        requestResult.Buffer = playersResponse.SerializeResponse();
        requestResult.NewHandler = this;

        return requestResult;
    }

    public RequestResult Signout()
    {
        RequestResult requestResult = new RequestResult();
        LogoutResponse logout = new LogoutResponse();

        LoginManager.GetInstance().Logout(user.GetUsername());

        logout.Status = 1;
        requestResult.Buffer = logout.SerializeResponse();
        requestResult.NewHandler = new LoginRequestHandler();

        return requestResult;
    }

    public RequestResult GetTopWinners()
    {
        RequestResult requestResult = new RequestResult();

        GetTopWinnersResponse highScore = new GetTopWinnersResponse();
        highScore.Winners = DBConnector.GetTopWinnersWithGamesPlayed();
        highScore.Status = 1;

        requestResult.Buffer = highScore.SerializeResponse();
        requestResult.NewHandler = this;

        return requestResult;
    }

    public RequestResult GetTopCategories()
    {
        RequestResult requestResult = new RequestResult();

        GetTopCategoriesResponse topCategories = new GetTopCategoriesResponse();
        topCategories.topCategories = DBConnector.GetTopSelectedCategories();

        requestResult.Buffer = topCategories.SerializeResponse();
        requestResult.NewHandler = this;

        return requestResult;
    }

    public RequestResult JoinRoom(RequestInfo requestInfo)
    {
        RequestResult requestResult = new RequestResult();
        JoinRoomResponse joinRoomResponse = new JoinRoomResponse();

        JoinRoomRequest joinRoomRequest = JsonRequestPacketDeserializer.DeserializeJoinRoomRequest(requestInfo.Buffer);
        RoomAdminRequestHandler room = RoomManager.GetInstance().GetRoom(RoomManager.GetInstance().GetRoom(joinRoomRequest.RoomID));

        if (!room.GetRoom().GetMetaData().IsPublic) // If private
        {
            user.AddRoom(room);

            if (room.HasUserBeenApproved(user) != (int)states.fail)
            {
                room.AddToList(user);
            }

            joinRoomResponse.Status = 0;
            requestResult.NewHandler = this;
        }
        else
        {
            if (room?.HasUserBeenApproved(user) == (int)states.fail)
            {
                joinRoomResponse.Status = 0;
            }
            else
            {
                room.GetRoom()?.AddUser(user);
                joinRoomResponse.Status = 1;
            }

            requestResult.NewHandler = new RoomMemberRequestHandler(RoomManager.GetInstance(), user, room.GetRoom());
        }

        requestResult.Buffer = joinRoomResponse.SerializeResponse();
        return requestResult;
    }

    public RequestResult CreateRoom(RequestInfo requestInfo)
    {
        RequestResult requestResult = new RequestResult();

        CreateRoomRequest newRoom = JsonRequestPacketDeserializer.DeserializeCreateRoomRequest(requestInfo.Buffer);
        RoomData roomData = new RoomData
        {
            Name = newRoom.RoomName,
            MaxPlayers = newRoom.MaxUsers,
            TimePerQuestion = newRoom.AnswerTimeout,
            NumOfQuestionsInGame = newRoom.QuestionCount,
            IsPublic = newRoom.Privacy.ToLower() == "public",
            Category = newRoom.Category,
            RoomOwner = user.GetUsername(),
            Id = RoomManager.GetInstance().GetFreeId(),
            CurrentAmountOfPlayers = 1,
            IsActive = 0
        };

        RoomManager.GetInstance().CreateRoom(user, roomData);

        CreateRoomResponse roomResponse = new CreateRoomResponse
        {
            Status = 1
        };

        requestResult.Buffer = roomResponse.SerializeResponse();

        RoomAdminRequestHandler admin = new RoomAdminRequestHandler(RoomManager.GetInstance(), user, RoomManager.GetInstance().GetRoom(roomData.Id));
        RoomManager.GetInstance().AddRoomAdmin(admin);
        requestResult.NewHandler = admin;

        return requestResult;
    }

    public RequestResult GetPersonalStats()
    {
        RequestResult requestResult = new RequestResult();

        GetPersonalStatsResponse response = new GetPersonalStatsResponse
        {
            Statistics = DBConnector.GetPersonalStat(user.GetUsername()),
        };

        requestResult.Buffer = response.SerializeResponse();
        requestResult.NewHandler = this;

        return requestResult;
    }

    public RequestResult HasApproved()
    {
        RequestResult requestResult = new RequestResult();
        HasApprovedResponse response = new HasApprovedResponse();

        if (RoomManager.GetInstance().GetRoom(user.GetWaitingRoom()) == null)
        {
            user.ClearRoomList();
            requestResult.NewHandler = this;
            response.ApprovedCode = 9;
            requestResult.Buffer = response.SerializeResponse();
            return requestResult;
        }

        bool approved = RoomManager.GetInstance().GetRoom(user.GetWaitingRoom()).HasUserBeenApproved(user) != 0;

        if (notApprovedRooms.Contains(user.GetWaitingRoom()))
        {
            requestResult.NewHandler = this;
            response.ApprovedCode = (int)states.fail;
            requestResult.Buffer = response.SerializeResponse();
            return requestResult;
        }

        RoomAdminRequestHandler temp = RoomManager.GetInstance().GetRoom(user.GetWaitingRoom());
        response.ApprovedCode = (RoomManager.GetInstance().GetRoom(user.GetWaitingRoom())).HasUserBeenApproved(user);
        requestResult.Buffer = response.SerializeResponse();

        if (response.ApprovedCode == (int)states.success)
        {
            requestResult.NewHandler = new RoomMemberRequestHandler(RoomManager.GetInstance(), user, user.GetWaitingRoom());
            user.ClearRoomList();
        }
        else
        {
            if (response.ApprovedCode == (int)states.fail)
            {
                notApprovedRooms.Add(user.GetWaitingRoom());
                user.ClearRoomList();
            }

            requestResult.NewHandler = this;
        }

        return requestResult;
    }

    public RequestResult StopWaiting()
    {
        RequestResult requestResult = new RequestResult();
        StopWaitingResponse response = new StopWaitingResponse();

        user.ClearRoomList();
        response.Succeded = true;

        requestResult.Buffer = response.SerializeResponse();
        requestResult.NewHandler = this;

        return requestResult;
    }
}
