﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;

public interface IRequestHandler
{
    RequestResult HandleRequest(RequestInfo requestInfo);
    void HandleProgramClosed();
}
