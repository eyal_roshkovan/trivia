﻿using System;
using System.Collections.Generic;
using System.Linq;

public class RoomAdminRequestHandler : IRequestHandler
{
    private Room room;
    private LoggedUser user;
    private RoomManager roomManager;
    private List<LoggedUser> usersWaiting;
    private List<LoggedUser> notApprovedUsers;

    public RoomAdminRequestHandler(RoomManager roomManager, LoggedUser user, Room room)
    {
        this.roomManager = roomManager;
        this.user = user;
        this.room = room;
        usersWaiting = new List<LoggedUser>();
        notApprovedUsers = new List<LoggedUser>();
    }

    public bool IsRequestRelevant(RequestInfo information)
    {
        return information.CodeID == TypesOfRequest.CLOSE_ROOM_CODE || information.CodeID == TypesOfRequest.GET_ROOM_STATE_CODE || information.CodeID == TypesOfRequest.START_GAME_CODE;
    }

    public RequestResult HandleRequest(RequestInfo information)
    {
        try 
        {
            RequestResult requestResult = new RequestResult();

            switch (information.CodeID)
            {
                case TypesOfRequest.ERROR_CODE:
                    HandleProgramClosed();
                    requestResult.NewHandler = new MenuRequestHandler(user);
                    break;

                case TypesOfRequest.CLOSE_ROOM_CODE:
                    requestResult = CloseRoom();
                    break;

                case TypesOfRequest.GET_ROOM_STATE_CODE:
                    requestResult = GetRoomState();
                    break;

                case TypesOfRequest.START_GAME_CODE:
                    requestResult = StartGame();
                    break;

                case TypesOfRequest.GET_OWNER_ROOM_CODE:
                    requestResult = GetOwnerRoom();
                    break;

                case TypesOfRequest.PLAYERS_IN_ROOM_CODE:
                    requestResult = GetPlayers();
                    break;

                case TypesOfRequest.GET_WAITING_TO_APPROVE_LIST_CODE:
                    requestResult = GetWaitingUsers();
                    break;

                case TypesOfRequest.APPROVE_USER_CODE:
                    requestResult = ApproveUser(information);
                    break;

                case TypesOfRequest.KICK_PLAYER_CODE:
                    requestResult = KickPlayer(information);
                    break;

                case TypesOfRequest.REFRESH_WEB_CODE:
                    requestResult = WebRefresh();
                    break;

                case TypesOfRequest.GET_IS_OWNER:
                    requestResult = GetIsOwner();
                    break;
            }

            return requestResult;
        }
        catch(Exception e)
        {
            Console.WriteLine(e);
            throw e;
        }
        
    }

    public Room GetRoom()
    {
        return room;
    }

    public void AddToList(LoggedUser user)
    {
        usersWaiting.Add(user);
    }
    private RequestResult GetIsOwner()
    {
        GetIsOwnerResponse getIsOwnerResponse = new GetIsOwnerResponse { IsOwner = true };
        RequestResult result = new RequestResult();
        result.NewHandler = this;
        result.Buffer = getIsOwnerResponse.SerializeResponse();
        return result;
    }
    public int HasUserBeenApproved(LoggedUser user)
    {
        if (usersWaiting.Contains(user))
            return -1;

        if (notApprovedUsers.Contains(user))
            return (int)states.fail;

        return (int)states.success;
    }

    public void RemoveUser(string username)
    {
        usersWaiting.RemoveAll(user => user.GetUsername() == username);
    }

    public void HandleProgramClosed()
    {
        CloseRoom();
    }
    private RequestResult WebRefresh()
    {
        RequestResult result = new RequestResult();
        WebAdminRefreshResponse response = new WebAdminRefreshResponse();
        response.usersWaiting = new GetWaitingToApproveListResponse();
        response.usersWaiting.Users = usersWaiting;
        response.PlayersInRoom = new GetPlayersInRoomResponse();
        response.PlayersInRoom.Users = room.GetAllUsers();
        result.Buffer = response.SerializeResponse();
        result.NewHandler = this;
        return result;
    }
    private RequestResult StartGame()
    {
        StartGameResponse response = null;
        RequestResult requestResult = new RequestResult();
        if(roomManager.GetRoom(room.GetMetaData().Id).GetAllUsers().Count > 1)
        {
            roomManager.GetRoom(this.room.GetMetaData().Id)?.SetActive(1);
            Room room = roomManager.GetRoom(this.room.GetMetaData().Id);
            requestResult.NewHandler = new GameRequestHandler(user, room);
            response = new StartGameResponse { Status = 1 };
        }
        else
        {
            requestResult.NewHandler = this;
            response = new StartGameResponse { Status = 0 };
        }
        // New handler
        requestResult.Buffer = response.SerializeResponse();

        return requestResult;
    }
    
    private RequestResult CloseRoom()
    {
        RequestResult requestResult = new RequestResult();
        CloseRoomResponse response = new CloseRoomResponse();

        roomManager.DeleteRoom((int)room.GetMetaData().Id);
        roomManager.DeleteRoom(this);
        response.Status = (int)states.success;
        requestResult.Buffer = response.SerializeResponse();
        requestResult.NewHandler = new MenuRequestHandler(user);

        return requestResult;
    }

    public RequestResult GetRoomState()
    {
        RequestResult requestResult = new RequestResult();
        GetRoomStateResponse roomState = new GetRoomStateResponse();

        roomState.AnswerTimeout = room.GetMetaData().TimePerQuestion;
        roomState.HasGameBegun = room.GetMetaData().IsActive != 0;
        roomState.Players = room.GetAllUsers();
        roomState.QuestionCount = room.GetMetaData().NumOfQuestionsInGame;

        roomState.Status = (int)states.success;
        requestResult.Buffer = roomState.SerializeResponse();

        requestResult.NewHandler = this;

        return requestResult;
    }

    public RequestResult GetOwnerRoom()
    {
        GetOwnerRoomResponse response = new GetOwnerRoomResponse();
        RequestResult result = new RequestResult();
        response.Room = room;
        result.Buffer = response.SerializeResponse();
        result.NewHandler = this;
        return result;
    }

    public RequestResult GetPlayers()
    {
        GetPlayersInRoomResponse response = new GetPlayersInRoomResponse();
        RequestResult result = new RequestResult();
        response.Users = room.GetAllUsers();
        result.Buffer = response.SerializeResponse();
        result.NewHandler = this;
        return result;
    }

    public RequestResult GetWaitingUsers()
    {
        RequestResult result = new RequestResult();
        GetWaitingToApproveListResponse response = new GetWaitingToApproveListResponse();
        response.Users = usersWaiting;
        result.NewHandler = this;
        result.Buffer = response.SerializeResponse();
        return result;
    }

    public RequestResult ApproveUser(RequestInfo information)
    {
        RequestResult result = new RequestResult();
        ApprovedUserRequest request = JsonRequestPacketDeserializer.DeserializeApprovedUserRequest(information.Buffer);
        if (request.Approved && !room.IsFull())
        {
            AddUser(request.Username);
        }
        else
        {
            DisapproveUser(request.Username);
        }
        ApprovedUserResponse response = new ApprovedUserResponse { Status = (int)states.success };
        result.Buffer = response.SerializeResponse();
        RemoveUser(request.Username);
        result.NewHandler = this;
        return result;
    }

    public RequestResult KickPlayer(RequestInfo information)
    {
        RequestResult requestResult = new RequestResult();
        KickPlayerResponse response = new KickPlayerResponse();
        KickPlayerRequest kickRequest = JsonRequestPacketDeserializer.DeserializeKickPlayerRequest(information.Buffer);
        try
        {
            DisapproveUser(kickRequest.Username);
            response.Success = true;
        }
        catch (Exception)
        {
            response.Success = false;
        }
        requestResult.Buffer = response.SerializeResponse();
        requestResult.NewHandler = this;
        return requestResult;
    }

    public void DisapproveUser(string username)
    {
        LoggedUser userToRemove = usersWaiting.FirstOrDefault(user => user.GetUsername() == username);

        if (userToRemove == null)
        {
            foreach (var user in room.GetAllUsers())
            {
                if (user.Key.GetUsername() == username)
                {
                    userToRemove = user.Key;
                    room.RemoveUser(userToRemove);
                    break;
                }
            }
        }

        if (userToRemove != null)
        {
            notApprovedUsers.Add(userToRemove);
        }
    }

    public void AddUser(string username)
    {
        LoggedUser userToAdd = usersWaiting.FirstOrDefault(user => user.GetUsername() == username);

        if (userToAdd != null)
        {
            room.AddUser(userToAdd);
        }
    }
}
