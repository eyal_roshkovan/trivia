﻿import { CodeToBinary, LengthToBinary, StringToBinary, OneIntResponseDeserializer, SendMessage, byteSize } from '../../Helper.js';

const signupCode = 2


function verifySignUp() {
    // Get form elements by their IDs
    var password = document.querySelector('input[type="password"]');
    var confirmPassword = document.querySelectorAll('input[type="password"]')[1];
    var email = document.querySelector('input[type="email"]');
    var termsCheckBox = document.querySelector('#TermsCheckBox');

    // Check if the password and confirm password match
    if (password.value !== confirmPassword.value) {
        alert("Your passwords don't match");
        return false;
    }

    // Check if the terms and conditions checkbox is checked
    if (!termsCheckBox.checked) {
        alert("You must accept the terms in order to sign up");
        return false;
    }

    // Check if the password is valid
    if (!isPasswordValid(password.value)) {
        alert("The password should contain at least 8 characters, with at least 1 capital letter and at least 1 digit");
        return false;
    }

    // Check if the email is valid
    if (!isEmailValid(email.value)) {
        alert("Invalid email address.");
        return false;
    }

    
    return true; // Return true to allow form submission
}

function isEmailValid(email) {
    var pattern = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
    return pattern.test(email);
}

function isPasswordValid(password) {
    if (password.length < 8) {
        return false;
    }
    if (!/[A-Z]/.test(password)) {
        return false;
    }
    if (!/\d/.test(password)) {
        return false;
    }
    return true;
}

async function TrySignup(username, password, email) {
    const jsonFormat = {
        username,
        password,
        email
    };

    const jsonString = JSON.stringify(jsonFormat);

    let newJsonString = "";
    for (let i = 0; i < jsonString.length; i++) {
        newJsonString += StringToBinary(jsonString[i]);
    }

    const len = byteSize * jsonString.length;
    const Code = CodeToBinary(signupCode);
    const lengthMessage = LengthToBinary(len);

    const message = Code + lengthMessage + newJsonString;
    let response = await SendMessage(message);
    const isOK = OneIntResponseDeserializer(response) === 1;
    return isOK; // Pass the result to the callback function
}


document.addEventListener('DOMContentLoaded', () => {
    const signup = document.getElementById('signupButton');

    signup.addEventListener('click', (event) => {
        event.preventDefault(); // Prevent the default form submission behavior
        if (verifySignUp()) {

            const username = document.querySelector('input[type="text"]').value;
            const password = document.querySelector('input[type="password"]').value;
            const email = document.querySelector('input[type="email"]').value;

            if (TrySignup(username, password, email)) {
                localStorage.setItem('username', username);
                CloseSocket();
                window.location.href = "../Menu/Menu.html";
            }
            else {
                alert("couldn't signup, there's probably a user with your username already, please try another username");
            }
        }
    });
});