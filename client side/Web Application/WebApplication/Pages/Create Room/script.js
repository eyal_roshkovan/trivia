﻿// Import statements
import { CodeToBinary, SendMessage, OneIntResponseDeserializer, StringToBinary, LengthToBinary, byteSize, CloseSocket } from '../../Helper.js';

const createRoomCode = 5;

// Function to validate the form data
function validateFormData() {
    // Get values from form input fields
    const roomName = document.getElementById("room-name").value;
    const timePerQuestion = document.getElementById("time-per-question").value;
    const participantsLimit = document.getElementById("participants-limit").value;
    const questionCount = document.getElementById("question-count").value;
    const category = document.getElementById("category").value;
    const privacy = document.getElementById("privacy").value;

    // You can add more specific validation rules as needed

    // Check if Room Name is empty
    if (roomName.trim() === "") {
        alert("Room Name is required.");
        return false;
    }

    // Check if Time per Question is a positive number
    if (isNaN(timePerQuestion) || timePerQuestion <= 0) {
        alert("Time per Question must be a positive number.");
        return false;
    }

    // Check if Participants Limit is a positive number
    if (isNaN(participantsLimit) || participantsLimit <= 1) {
        alert("Participants Limit must be at least 2.");
        return false;
    }

    // Check if Question Count is a positive number
    if (isNaN(questionCount) || questionCount <= 0) {
        alert("Question Count must be a positive number.");
        return false;
    }

    // Check if a category is selected
    if (category === "") {
        alert("Please select a category.");
        return false;
    }

    // Check if privacy setting is selected
    if (privacy === "") {
        alert("Please select the privacy setting.");
        return false;
    }

    return true;
}

// Function to try creating a room
async function TryCreateRoom() {
    try {
        if (!validateFormData()) {
            // Handle validation failure, e.g., show an error message
            return false;
        }

        const roomName = document.getElementById("room-name").value;
        const maxUsers = parseInt(document.getElementById("participants-limit").value, 10);
        const answerTimeout = parseInt(document.getElementById("time-per-question").value, 10);
        const questionCount = parseInt(document.getElementById("question-count").value, 10);
        const category = document.getElementById("category").value;
        const privacy = document.getElementById("privacy").value;

        const jsonFormat = {
            roomName,
            category,
            maxUsers,
            questionCount,
            answerTimeout,
            privacy
        };

        const jsonString = JSON.stringify(jsonFormat);
        const newJsonString = Array.from(jsonString, char => StringToBinary(char)).join("");

        const len = byteSize * jsonString.length;
        const code = CodeToBinary(createRoomCode);
        const lengthMessage = LengthToBinary(len);

        const message = code + lengthMessage + newJsonString;

        const response = await SendMessage(message);

        // Assuming OneIntResponseDeserializer returns 1 for success
        const isOK = OneIntResponseDeserializer(response) === 1;

        return isOK;
    } catch (error) {
        console.error('Error:', error);
        // Handle the error, e.g., show an error message to the user
        throw error;
    }
}


// Event listener when the DOM is loaded
document.addEventListener('DOMContentLoaded', () => {
    const form = document.getElementById('my-form');
    const goBackButton = document.getElementById("goBackButton");
    form.addEventListener('submit', async (event) => {
        try {
            // Assuming TryCreateRoom is an asynchronous function that returns a promise
            let isCreateOK = await TryCreateRoom();
            if (isCreateOK) {
                // Move to waiting room
                CloseSocket();
                const answerTimeout = parseInt(document.getElementById("time-per-question").value, 10);
                const questionCount = parseInt(document.getElementById("question-count").value, 10);
                localStorage.setItem('timePerQuestion', answerTimeout);
                localStorage.setItem('questionCount', questionCount);
                window.location.href = "../Waiting Room/WaitingRoom.html";
            }
        } catch (error) {
            console.error('Error:', error);
            // Handle the error if needed
        }
    });

    goBackButton.addEventListener("click", function () {
        // Navigate to the Menu.html page
        CloseSocket();
        window.location.href = "../Menu/Menu.html";
    });
});
