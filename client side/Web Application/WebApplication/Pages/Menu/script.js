﻿import { CloseSocket, CodeToBinary, SendMessage} from '../../Helper.js';
const logOutCode = 3;


async function logout() {
    await SendMessage(CodeToBinary(logOutCode));
}

function showConfirmationModal() {
    const modal = document.getElementById("confirmation-modal");
    modal.style.display = "block";

    // Add event listeners to the "Yes" and "No" buttons
    const modalYesButton = document.getElementById("modal-yes");
    modalYesButton.addEventListener("click", function () {
        // Handle the "Yes" button action here
        // Send Logout to the user
        modal.style.display = "none"; // Close the modal
        logout();
        CloseSocket();
        window.location.href = '../Login/Login.html';
    });

    const modalNoButton = document.getElementById("modal-no");
    modalNoButton.addEventListener("click", function () {
        // Handle the "No" button action here
        modal.style.display = "none"; // Close the modal
    });
}

document.addEventListener("DOMContentLoaded", function () {
    const joinRoomButton = document.getElementById("JoinRoomButton");
    const welcomeLabel = document.getElementById("welcomeLabel");

    const storedUsername = localStorage.getItem('username');
    welcomeLabel.textContent = "Welcome " + storedUsername;


    joinRoomButton.addEventListener("click", function () {
        CloseSocket();
        window.location.href = "../Join Room/JoinRoom.html";
    });

    const createRoomButton = document.getElementById("CreateRoomButton");
    createRoomButton.addEventListener("click", function () {
        CloseSocket();
        window.location.href = "../Create Room/CreateRoom.html";
    });

    const statisticsPageButton = document.getElementById("StatisticsPage");
    statisticsPageButton.addEventListener("click", function () {
        CloseSocket();
        window.location.href = "../Statistics/Statistics.html";
    });

    const signOutButton = document.getElementById("SignoutButton");
    signOutButton.addEventListener("click", function () {
        showConfirmationModal();
    });
})
