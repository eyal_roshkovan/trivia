﻿let socket = new WebSocket('ws://127.0.0.1:8829');
const byteSize = 8; // Assign an appropriate value to byteSize
const TIMEOUT = 5000000; // Adjust the timeout value as needed (in milliseconds)

function GenerateSerialNumber() {
    const oldSerialNumber = localStorage.getItem('serialNumber');

    if (oldSerialNumber === null) { // If not exists

        let serialNumber = '';

        for (let i = 0; i < 10; i++) {
            const digit = Math.floor(Math.random() * 10); // Generate a random digit between 0 and 9
            serialNumber += digit;
        }

        return serialNumber;
    } else {
        return oldSerialNumber;
    }
}
function restartSocket() {
    CloseSocket();
    socket = new WebSocket('ws://127.0.0.1:8828');
}
function CloseSocket() {
    socket.close();
}

socket.onopen = () => {
    console.log('Connected to the server');
};

socket.onclose = (event) => {
    if (event.wasClean) {
        console.log('Connection closed cleanly');
    } else {
        console.error(`Connection abruptly closed: ${event.reason}`);
    }
};

socket.onerror = (error) => {
    console.error(`WebSocket Error: ${error.message}`);
};

function checkSocketReadyState() {
    if (socket.readyState === WebSocket.CONNECTING) {
        return "WebSocket is connecting";
    } else if (socket.readyState === WebSocket.OPEN) {
        return "WebSocket is open";
    } else if (socket.readyState === WebSocket.CLOSING) {
        return "WebSocket is closing";
    } else if (socket.readyState === WebSocket.CLOSED) {
        return "WebSocket is closed";
    } else {
        return "WebSocket state is unknown";
    }
}

// Function to send a message to the server and return the response
async function SendMessage(message) {
    return new Promise((resolve, reject) => {
        if (socket.readyState === WebSocket.OPEN) {
            const serialNumber = localStorage.getItem('serialNumber');
            message = serialNumber + message;

            // Set up a one-time event listener for the response
            const responseHandler = (event) => {
                resolve(event.data);
                // Remove the event listener to avoid memory leaks
                socket.removeEventListener('message', responseHandler);
                socket.removeEventListener('error', errorHandler);
            };

            // Set up an error handler in case of issues
            const errorHandler = (errorEvent) => {
                reject(errorEvent);
                // Remove the event listener to avoid memory leaks
                socket.removeEventListener('message', responseHandler);
                socket.removeEventListener('error', errorHandler);
            };

            // Add event listeners before sending the message
            socket.addEventListener('message', responseHandler);
            socket.addEventListener('error', errorHandler);

            // Send the message after setting up event listeners
            socket.send(message);
        } else {
            reject('WebSocket is not open yet.');
        }
    });
}




/**
 * Converts a binary string to a JSON string and replaces '\\n' with newline characters.
 * @param {string} binary - The input binary string.
 * @returns {string} - The JSON string with newline characters.
 */
function GetJsonFromBinary(binary) {
    let current = ""; // Initialize a variable to store the current 8-bit binary chunk.
    let json = "";    // Initialize the resulting JSON string.

    // Loop through the binary string in 8-bit chunks.
    for (let i = 0; i < binary.length; i += 8) {
        for (let j = 0; j < 8; j++) {
            current += binary[i + j]; // Construct the 8-bit chunk.
        }
        if (json.length === 0) {
            // If the JSON string is empty and the current chunk represents an opening brace '{', add it to the JSON.
            if (BinaryToChar(current) === '{') {
                json += BinaryToChar(current);
            }
        } else {
            // For subsequent chunks, directly add them to the JSON.
            json += BinaryToChar(current);
        }
        current = ""; // Reset the current chunk.
    }
    return json; // Replace '\\n' with newline characters.
}

/**
 * Deserializes a message to extract an integer value.
 * @param {string} message - The input message containing binary data.
 * @returns {number} - The extracted integer value.
 */
function OneIntResponseDeserializer(message) {
    let content = "";      // Initialize a variable to store the binary content.
    let binaryChar = "";   // Initialize a variable to store the current 8-bit binary chunk.

    // Loop through the message starting from the 9th character (skip the first 8 bits which typically don't contain data).
    for (let i = 8; i < message.length; i += 8) {
        for (let j = 0; j < 8; j++) {
            binaryChar += message[i + j]; // Construct the 8-bit chunk.
        }
        if (content.length === 0 && BinaryToChar(binaryChar) === '{') {
            // If the content is empty and the current chunk represents an opening brace '{', add it to the content.
            content += '{';
        } else if (content.length > 0) {
            // For subsequent chunks, directly add them to the content.
            content += BinaryToChar(binaryChar);
        }
        binaryChar = ""; // Reset the current chunk.
    }

    // Parse the content as JSON and extract the 'status' property as an integer.
    const jsonDocument = JSON.parse(content);
    const root = jsonDocument;
    if (root.hasOwnProperty('approved')) {
        return root.approved;
    }
    return root.status;
}

/**
 * Deserializes a message to extract a boolean value.
 * @param {string} message - The input message containing binary data.
 * @returns {boolean} - The extracted boolean value.
 */
function OneBoolResponseDeserializer(message) {
    let content = "";      // Initialize a variable to store the binary content.
    let binaryChar = "";   // Initialize a variable to store the current 8-bit binary chunk.

    // Loop through the message starting from the 9th character (skip the first 8 bits which typically don't contain data).
    for (let i = 8; i < message.length; i += 8) {
        for (let j = 0; j < 8; j++) {
            binaryChar += message[i + j]; // Construct the 8-bit chunk.
        }
        if (content.length === 0 && BinaryToChar(binaryChar) === '{') {
            // If the content is empty and the current chunk represents an opening brace '{', add it to the content.
            content += '{';
        } else if (content.length > 0) {
            // For subsequent chunks, directly add them to the content.
            content += BinaryToChar(binaryChar);
        }
        binaryChar = ""; // Reset the current chunk.
    }

    // Parse the content as JSON and extract the 'approved' or 'hasEveryoneFinished' property as a boolean.
    const root = JSON.parse(content);
    try
    {
        return root.approved;
    }
    catch
    {
        try
        {
            return root.hasEveryoneFinished;
        }
        catch
        {
            return root.isOwner;
        }
    }
}

/**
 * Converts a string to its binary representation.
 * @param {string} s - The input string to be converted to binary.
 * @returns {string} - The binary representation of the input string.
 */
function StringToBinary(s) {
    // Initialize a builder to store binary characters.
    let binaryBuilder = "";
    for (let i = 0; i < s.length; i++) {
        // Convert each character in the string to its binary representation (8 bits) and append to the binary string.
        const binaryChar = s.charCodeAt(i).toString(2).padStart(8, '0');
        binaryBuilder += binaryChar;
    }
    // Return the binary string.
    return binaryBuilder;
}

/**
 * Converts an integer to its binary representation (8 bits).
 * @param {number} n - The input integer to be converted to binary.
 * @returns {string} - The binary representation of the input integer.
 */
function CodeToBinary(n) {
    // Convert the number to its binary representation (8 bits) and pad with leading zeros if necessary.
    return n.toString(2).padStart(8, '0');
}

/**
 * Converts an integer to its binary representation (32 bits).
 * @param {number} n - The input integer to be converted to binary.
 * @returns {string} - The binary representation of the input integer (32 bits).
 */
function LengthToBinary(n) {
    // Convert the number to its binary representation (32 bits) and pad with leading zeros if necessary.
    return n.toString(2).padStart(32, '0');
}

/**
 * Converts a binary string to a character.
 * @param {string} binaryString - The binary string representing a character.
 * @returns {string} - The character represented by the binary string.
 */
function BinaryToChar(binaryString) {
    // Convert the binary string to a decimal value and then to a character.
    const decimalValue = parseInt(binaryString, 2);
    const character = String.fromCharCode(decimalValue);
    return character;
}

GenerateSerialNumber()

export {
    CloseSocket,
    checkSocketReadyState,
    SendMessage,
    GetJsonFromBinary,
    OneIntResponseDeserializer,
    OneBoolResponseDeserializer,
    StringToBinary,
    CodeToBinary,
    LengthToBinary,
    BinaryToChar,
    byteSize,
    restartSocket
};
