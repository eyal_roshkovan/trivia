class Room {
    constructor(roomName, roomOwner, isPublic, category, currentPlayers, maxPlayers, id, amountOfQuestions, timePerQuestion) {
        this.roomName = roomName;
        this.roomOwner = roomOwner;
        this.isPublic = isPublic;
        this.category = category;
        this.currentPlayers = currentPlayers;
        this.maxPlayers = maxPlayers;
        this.id = id;
        this.amountOfQuestions = amountOfQuestions;
        this.timePerQuestion = timePerQuestion;
    }
}

class Result {
    constructor(username, rightAnswers, wrongAnswers, averageTimeToAnswer) {
        this.username = username;
        this.rightAnswers = rightAnswers;
        this.wrongAnswers = wrongAnswers;
        this.averageTimeToAnswer = averageTimeToAnswer;
    }
}

class UserData {
    constructor(username, points, roll, ready) {
        this.username = username;
        this.points = points;
        this.roll = roll;
        this.ready = ready;
    }
}

export { Room, Result, UserData };