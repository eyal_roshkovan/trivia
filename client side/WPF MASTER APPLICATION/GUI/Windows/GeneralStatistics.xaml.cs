﻿using GUI.ProgramManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GUI.Windows
{
    /// <summary>
    /// Interaction logic for GeneralStatistics.xaml
    /// </summary>
    public partial class GeneralStatistics : Window
    {
        public GeneralStatistics()
        {
            InitializeComponent();
            MyGrid.Background = Manager.background;
            Update();
        }
        public void Update()
        {
            UpdateTopWinnersTable();
            UpdateTopCategoriesTable();
        }
        // Update the Top Winners table
        private void UpdateTopWinnersTable()
        {
            List<Tuple<string, int>> topWinners = MenuManager.GetTopWinners();

            // Assuming TopWinnersDataGrid is the name of your DataGrid for Top Winners
            TopWinnersDataGrid.ItemsSource = topWinners;
        }

        // Update the Top Categories table
        private void UpdateTopCategoriesTable()
        {
            List<Tuple<string, int>> topCategories = MenuManager.GetTopSelectedCategories();

            // Assuming TopCategoriesDataGrid is the name of your DataGrid for Top Categories
            TopCategoriesDataGrid.ItemsSource = topCategories;
        }

        // Replace this with your actual method to execute SQL queries and retrieve data


        // Replace this with your actual method to execute SQL queries and retrieve data


        // Handle the Back button click event
        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            MenuManager.ShowMenu();
        }
    }
}
