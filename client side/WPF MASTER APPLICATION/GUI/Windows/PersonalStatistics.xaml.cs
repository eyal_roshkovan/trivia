﻿using GUI.ProgramManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Text.Json;
using System.Xml.Linq;
using System.Collections;

namespace GUI.Windows
{
    /// <summary>
    /// Interaction logic for PersonalStatistics.xaml
    /// </summary>
    public partial class PersonalStatistics : Window
    {
        public PersonalStatistics()
        {
            InitializeComponent();
            MyGrid.Background = Manager.background;
            Icon = Manager.logo;
            Closing += Manager.HandleClose;
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            MenuManager.ShowMenu();
        }
        public void Update()
        {
            List<string> stats = MenuManager.GetPersonalStats();
            UpdateData(stats);
        }
        private void UpdateData(List<string> stats)
        {
            GamesWonBlock.Text = stats[0];
            CreatedRoomsBlock.Text = stats[1];
            NumTotalTextBlock.Text = stats[2];
            NumGamesTextBlock.Text = stats[3];
        }
    }
}

