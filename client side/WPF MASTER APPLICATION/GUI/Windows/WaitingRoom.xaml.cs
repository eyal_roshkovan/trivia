﻿using GUI.ProgramManager;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Sockets;
using System.Numerics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace GUI.Windows
{
    public partial class WaitingRoom : Window
    {
        private string userName;
        private string roll;
        private int notReady;
        private Room room;
        public WaitingRoom(Room room, string username)
        {
            InitializeComponent();
            Closing += Manager.HandleClose;
            this.room = room;
            Icon = Manager.logo;
            MyGrid.Background = Manager.background;
            userName = username;
            roll = room.RoomOwner == username ? "Owner" : "Member";
            if (roll == "Owner")
            {
                ReadyUnready.Content = "Start Game !";
                GoBack.Content = "Close Room !";
                ReadyUnready.IsEnabled = false;
                ReadyUnready.Background = new SolidColorBrush(Colors.Gray);
                Thread owner = new Thread(new ThreadStart(ManageWaitingUsers));
                owner.Start();
            }
            Thread thread = new Thread(new ThreadStart(refresh));
            thread.Start();
        }

        public void refresh()
        {
            string response = "";
            while (WaitingRoomManager.waitingRoomActive)
            {
                if (roll != "Owner")
                {
                    response = WaitingRoomManager.SendAndRecv_Refresh();

                    if (response.Contains("2"))// game has been started
                    {
                        WaitingRoomManager.waitingRoomActive = false;
                        MessageBox.Show("Game begins in few moments, Get ready!", "Start game", MessageBoxButton.OK, MessageBoxImage.Information);
                        //Move game page
                        Dispatcher.Invoke(() =>
                        {
                            GameManager.ShowGameWindow(room, MenuManager.username);
                        });
                    
                    }
                    else if (response.Contains("1"))//No change
                    {
                        //Get List of players
                        UpdateListOfPlayers();
                        Thread.Sleep(100); // Sleep for 3 seconds

                    }
                    else if(response.Contains("9"))
                    {
                        Dispatcher.Invoke(() =>
                        {
                            MessageBox.Show("You have been kicked out of the room by the owner", "Error", MessageBoxButton.OK, MessageBoxImage.Information);
                            MenuManager.menuActive = true;
                            WaitingRoomManager.waitingRoomActive = false;
                            WaitingRoomManager.ShowMenu();
                        });
                    }
                    else
                    {
                        Dispatcher.Invoke(() =>
                        {
                            MessageBox.Show("The room has been closed by the owner", "Error", MessageBoxButton.OK, MessageBoxImage.Information);
                            MenuManager.menuActive = true;
                            WaitingRoomManager.waitingRoomActive = false;
                            WaitingRoomManager.ShowMenu();
                        });
                    }
                }
                else
                {
                    UpdateListOfPlayers();
                    Thread.Sleep(1000); // Sleep for 1 second
                }
            }
        }

        private void ManageWaitingUsers()
        {
            while (WaitingRoomManager.waitingRoomActive)
            {
                // 1) Recieve users
                UserData[] users = WaitingRoomManager.GetWaitingUsers().ToArray();

                // 2) Run through every user and ask the owner about the weather to approve the user or not
                for (int i = 0; i < users.ToArray().Length; i++)
                {
                    MessageBoxResult result = MessageBox.Show(users.ToArray()[i].UserName + " Wants to join !!!", "JOIN ROOM APPROVAL", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (result == MessageBoxResult.Yes)
                    {
                        WaitingRoomManager.ApproveUser(users.ToArray()[i].UserName, true);
                    }
                    else
                    {
                        WaitingRoomManager.ApproveUser(users.ToArray()[i].UserName, false);
                    }
                }
                Thread.Sleep(1000); // Sleep for 1 second

            }
        }
        public void UpdateListOfPlayers()
        {
            notReady = 0;
            UserData[] users = WaitingRoomManager.GetAllUsers().ToArray();
            GridView gridView = null;

            // Execute the update operation on the UI thread
            Dispatcher.Invoke(() =>
            {
                gridView = (GridView)listView.View;
                gridView.Columns.Clear();

                // Add columns to the GridView
                gridView.Columns.Add(new GridViewColumn
                {
                    Header = "Username",
                    DisplayMemberBinding = new Binding("Username"),
                    Width = 200
                });

                gridView.Columns.Add(new GridViewColumn
                {
                    Header = "Games Played",
                    DisplayMemberBinding = new Binding("TotalPoints"),
                    Width = 200
                });
                gridView.Columns.Add(new GridViewColumn
                {
                    Header = "Roll",
                    DisplayMemberBinding = new Binding("Roll"),
                    Width = 200
                });

                gridView.Columns.Add(new GridViewColumn
                {
                    Header = "Ready",
                    DisplayMemberBinding = new Binding("Ready"),
                    Width = 150
                });

                // Additional column with the "Kick" button
                var kickButtonTemplate = new DataTemplate();
                var kickButtonFactory = new FrameworkElementFactory(typeof(Button));
                kickButtonFactory.SetValue(ContentProperty, "Kick");
                kickButtonFactory.SetValue(WidthProperty, 1300.0);
                kickButtonFactory.SetValue(HeightProperty, 30.0);
                kickButtonFactory.AddHandler(Button.ClickEvent, new RoutedEventHandler(KickButton_Click));
                kickButtonFactory.SetBinding(Button.VisibilityProperty, new Binding("IsKickEnabled"));
                kickButtonFactory.SetBinding(Button.TagProperty, new Binding("UserObject"));
                kickButtonTemplate.VisualTree = kickButtonFactory;

                if (roll == "Member")
                {
                    kickButtonFactory.SetValue(Button.VisibilityProperty, Visibility.Hidden);
                }

                listView.ItemsSource = users.Select(user =>
                {
                    var username = user.UserName;
                    var totalPoints = user.Points;
                    var roll = user.Roll;
                    var UserObject = user;
                    var ready = user.Ready ? "Ready" : "Not Ready";
                    if (ready == "Not Ready")
                    {
                        notReady++;
                    }
                    var foregroundColor = username == userName ? Brushes.AliceBlue : Brushes.Black;

                    return new
                    {
                        Username = username,
                        TotalPoints = totalPoints,
                        Roll = roll,
                        Ready = ready,
                        UserObject = UserObject,
                        ForegroundColor = foregroundColor,
                        KickButtonTemplate = kickButtonTemplate
                    };
                });


                // Update the item container style to paint the foreground color and include the "Kick" button
                listView.ItemContainerStyle = new Style(typeof(ListViewItem));
                listView.ItemContainerStyle.Setters.Add(new Setter(ForegroundProperty, new Binding("ForegroundColor")));

                // Define the column with the "Kick" button
                gridView.Columns.Add(new GridViewColumn
                {
                    Header = "",
                    CellTemplate = kickButtonTemplate,
                    Width = 150
                });

                if (ReadyUnready.Content.ToString() == "Start Game !")
                {
                    if (notReady == 1 && users.Length > 1)
                    {
                        ReadyUnready.IsEnabled = true;
                        ReadyUnready.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#4CAF50"));
                    }
                    else
                    {
                        ReadyUnready.IsEnabled = false;
                        ReadyUnready.Background = new SolidColorBrush(Colors.Gray);
                    }
                }
            });
        }

        private void KickButton_Click(object sender, RoutedEventArgs e)
        {
            Button kickButton = (Button)sender;
            UserData user = (UserData)kickButton.Tag;
            if (user.UserName != userName)
            {
                WaitingRoomManager.KickPlayer(user);
            }
            else
            {
                MessageBox.Show("You can't kick yourself out of the room", "Message Box", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (roll == "Owner")
            {
                WaitingRoomManager.waitingRoomActive = false;
                WaitingRoomManager.SendAndRecv_CloseRoom();
            }
            else
            {
                if (ReadyUnready.Content.ToString() == "Unready") // If already ready
                {

                    MessageBoxResult result = MessageBox.Show("Are you sure you want to exit ?", "Message Box", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (result == MessageBoxResult.Yes)
                    {
                        WaitingRoomManager.waitingRoomActive = false;
                        WaitingRoomManager.SendAndRecv_LeaveRoom();
                    }
                    else
                    {
                        return;
                    }

                }
                else
                {
                    WaitingRoomManager.waitingRoomActive = false;
                    WaitingRoomManager.SendAndRecv_LeaveRoom();
                }
            }
            Dispatcher.Invoke(() =>
            {
                WaitingRoomManager.ShowMenu();
            });
        }

        private void ReadyUnready_Click(object sender, RoutedEventArgs e)
        {
            if (roll == "Owner")
            {
                WaitingRoomManager.waitingRoomActive = false;
                WaitingRoomManager.SendAndRecv_StartGame(room);
            }
            else
            {
                WaitingRoomManager.ReadyUnready();
                if (ReadyUnready.Content.ToString() == "Ready up !")
                {
                    ReadyUnready.Content = "Unready";
                    ReadyUnready.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#E91E63"));
                }
                else
                {
                    ReadyUnready.Content = "Ready up !";
                    ReadyUnready.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#4CAF50"));
                }
            }
        }
    }
}