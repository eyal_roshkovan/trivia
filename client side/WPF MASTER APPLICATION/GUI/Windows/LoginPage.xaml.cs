﻿using System.Windows;
using GUI.Views;
using System;
using GUI.ProgramManager;
using System.Windows.Media.Imaging;
using System.Reflection;
using System.IO;
using System.Windows.Media;
using System.Windows.Controls;

namespace GUI.Views
{
    public partial class LoginPage : Window
    {
        public static string RemoveBetween(string path)
        {
            string startPattern = "GUI\\GUI";
            string endPattern = "\\logo.png";

            int startIndex = path.IndexOf(startPattern);
            int endIndex = path.IndexOf(endPattern, startIndex + startPattern.Length);

            if (startIndex != -1 && endIndex != -1)
            {
                return path.Remove(startIndex + startPattern.Length, endIndex - (startIndex + startPattern.Length));
            }

            return path;
        }

        public LoginPage()
        {
            InitializeComponent();
            string logoPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\logo.png";
            string backgroundPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\background.jpg";
            Uri logoUri = new Uri(logoPath);
            BitmapImage logoBitmap = new BitmapImage(logoUri);

            Uri backgroundUri = new Uri(backgroundPath);
            BitmapImage backgrounBitmap = new BitmapImage(backgroundUri);
            Manager.logo = logoBitmap;
            ImageBrush imageBrush = new ImageBrush();
            imageBrush.ImageSource = new BitmapImage(new Uri(backgroundPath));
            Manager.background = imageBrush;
            MyGrid.Background = Manager.background;
            Icon = logoBitmap;
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            if(LoginOrSignupManager.TryLogin(txtUsername.Text, txtPassword.Password))
            {
                MessageBox.Show("Login successful. Welcome!", "Login Success", MessageBoxButton.OK, MessageBoxImage.Information);
                MenuManager.DefineAllWindows(txtUsername.Text);
                MenuManager.ShowMenu();
                return;
            }
            MessageBox.Show("Login failed. Please check your username and password and try again.", "Login Error", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void SignupButton_Click(object sender, RoutedEventArgs e)
        {
            LoginOrSignupManager.ShowSignup();
        }
    }
}