﻿using GUI.Views;
using System;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.Json;
using System.Windows;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace GUI.ProgramManager
{
    public class Manager : Application
    {
        protected const string serverIP = "127.0.0.1";
        protected const int byteSize = 8;
        protected static Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        protected static IPAddress ipAddress = IPAddress.Parse(serverIP); // IP address of the server
        protected static object lockObject = new object();
        protected const int bufferSize = 2048;
        public static BitmapImage logo;
        public static ImageBrush background;

        protected static string SendMessage(string message)
        {
            byte[] data = Encoding.ASCII.GetBytes(message);
            byte[] buffer = new byte[bufferSize];
            int bytesRead;
            string response = "";
            lock (lockObject)
            {
                socket.Send(data);
                do
                {
                    bytesRead = socket.Receive(buffer);
                    response += Encoding.ASCII.GetString(buffer, 0, bytesRead);
                } while (bytesRead == bufferSize);
            }
            return response.Replace('\0', '0').Replace('\u0001', '1');
        }
        public static void HandleClose(object sender, System.ComponentModel.CancelEventArgs e)
        {
            lock (lockObject)
            {
                socket.Send(Encoding.ASCII.GetBytes(CodeToBinary(0)));
                socket.Shutdown(SocketShutdown.Both);
                socket.Close();
            }
        }
        public static void Run(int port)
        {
            IPEndPoint endPoint = new IPEndPoint(ipAddress, port);
            socket.Connect(endPoint);
        }
        protected static string GetJsonFromBinary(string binary)
        {
            string current = "";
            string json = "";
            for (int i = 0; i < binary.Length; i += 8)
            {
                for (int j = 0; j < 8; j++)
                {
                    current += binary[i + j];
                }
                if (json.Length == 0)
                {
                    if (BinaryToChar(current) == '{')
                        json += BinaryToChar(current);
                }
                else
                {
                    json += BinaryToChar(current);
                }
                current = "";
            }
            return json.Replace("\\n", "\n");
        }
        protected static int OneIntResponseDeserializer(string message)
        {
            string content = "", binaryChar = "";

            for (int i = 8; i < message.Length; i += 8)
            {
                for (int j = 0; j < 8; j++)
                {
                    binaryChar += message[i + j];
                }
                if (content.Length == 0 && BinaryToChar(binaryChar) == '{')
                    content += '{';
                else if (content.Length > 0)
                    content += BinaryToChar(binaryChar);
                binaryChar = "";
            }

            var jsonDocument = JsonDocument.Parse(content);
            var root = jsonDocument.RootElement;
            try
            {
                return root.GetProperty("status").GetInt32();
            }
            catch
            {
                return root.GetProperty("approved").GetInt32();
            }
        }
        protected static bool OneBoolResponseDeserializer(string message)
        {
            string content = "", binaryChar = "";

            for (int i = 8; i < message.Length; i += 8)
            {
                for (int j = 0; j < 8; j++)
                {
                    binaryChar += message[i + j];
                }
                if (content.Length == 0 && BinaryToChar(binaryChar) == '{')
                    content += '{';
                else if (content.Length > 0)
                    content += BinaryToChar(binaryChar);
                binaryChar = "";
            }

            var jsonDocument = JsonDocument.Parse(content);
            var root = jsonDocument.RootElement;
            try
            {
                return root.GetProperty("approved").GetBoolean();
            }
            catch
            { 
                return root.GetProperty("hasEveryoneFinished").GetBoolean();
            }

        }

        protected static string StringToBinary(string s)
        {

            // Convert each character in the string to its binary representation
            StringBuilder binaryBuilder = new StringBuilder();
            foreach (char c in s)
            {
                string binaryChar = Convert.ToString(c, 2).PadLeft(8, '0');
                binaryBuilder.Append(binaryChar);
            }

            // Join the binary strings together to form a single binary string
            string binaryString = binaryBuilder.ToString();

            // Return the binary string
            return binaryString;
        }

        protected static string CodeToBinary(int n)
        {
            // Convert the number to its binary representation
            string binaryString = Convert.ToString(n, 2).PadLeft(8, '0');
            // Return the binary string
            return binaryString;
        }

        protected static string LengthToBinary(int n)
        {
            // Convert the number to its binary representation
            string binaryString = Convert.ToString(n, 2).PadLeft(32, '0');
            // Return the binary string
            return binaryString;
        }

        public static char BinaryToChar(string binaryString)
        {
            int decimalValue = Convert.ToInt32(binaryString, 2);
            char character = Convert.ToChar(decimalValue);
            return character;
        }
    }
}