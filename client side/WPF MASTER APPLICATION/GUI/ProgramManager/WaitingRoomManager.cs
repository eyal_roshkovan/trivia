﻿using GUI.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Xml.Linq;
using Newtonsoft.Json.Linq;
using System.Runtime.CompilerServices;

namespace GUI.ProgramManager
{
    public class MyData
    {
        public string Room { get; set; }
    }
    public class UserData
    {
        public string UserName { get; set; }
        public string Points { get; set; }
        public string Roll { get; set; }
        public bool Ready { get; set; }
        public UserData(string username, string points, string roll, bool ready)
        {
            UserName = username;
            Points = points;
            Roll = roll;
            Ready = ready;
        }
    }

    public class WaitingRoomManager : Manager
    {
        private const int readyUnreadyCode = 11;
        private const int getOwnerRoomCode = 17;
        private const int getPlayersInRoomCode = 7;
        private const int getWaitingUsersCode = 20;
        private const int approveUserCode = 21;
        private const int kickPlayerCode = 26;
        public static bool waitingRoomActive = true;
        private static string our_username;
        private static WaitingRoom waitingRoomWindow;
        public static void ShowWaitingRoom(Room room, string username)
        {
            our_username = username;
            waitingRoomActive = true;
            MenuManager.menuActive = false;
            MenuManager.CloseAllWindows();
            waitingRoomWindow = new WaitingRoom(room, username);
            waitingRoomWindow.Show();
        }
        public static void KickPlayer(UserData user)
        {
            var jsonFormat = new { username = user.UserName };
            string jsonString = JsonSerializer.Serialize(jsonFormat);
            string newJsonString = "";
            foreach (char c in jsonString)
            {
                newJsonString += StringToBinary(c.ToString());
            }
            int len = byteSize * jsonString.Length;
            string Code = CodeToBinary(kickPlayerCode);
            string lengthMessage = LengthToBinary(len);

            string message = Code + lengthMessage + newJsonString;

            // Not working yet
            SendMessage(message);
        }
        public static void ReadyUnready()
        {
            var jsonFormat = new { };
            string jsonString = JsonSerializer.Serialize(jsonFormat);
            string newJsonString = "";
            foreach (char c in jsonString)
            {
                newJsonString += StringToBinary(c.ToString());
            }
            int len = byteSize * jsonString.Length;
            string Code = CodeToBinary(readyUnreadyCode);
            string lengthMessage = LengthToBinary(len);

            string message = Code + lengthMessage + newJsonString;
            SendMessage(message);
        }
        public static void ShowMenu()
        {
            MenuManager.DefineAllWindows(our_username);
            MenuManager.ShowJoinRoom();
            Close();
        }
        public static void ApproveUser(string username, bool approved)
        {
            var jsonFormat = new { username = username, approved = approved };
            string jsonString = JsonSerializer.Serialize(jsonFormat);
            string newJsonString = "";
            foreach (char c in jsonString)
            {
                newJsonString += StringToBinary(c.ToString());
            }
            int len = byteSize * jsonString.Length;
            string Code = CodeToBinary(approveUserCode);
            string lengthMessage = LengthToBinary(len);

            string message = Code + lengthMessage + newJsonString;
            SendMessage(message);
        }
        public static Room GetRoom()
        {
            var jsonFormat = new { };
            string jsonString = JsonSerializer.Serialize(jsonFormat);
            string newJsonString = "";
            foreach (char c in jsonString)
            {
                newJsonString += StringToBinary(c.ToString());
            }
            int len = byteSize * jsonString.Length;
            string Code = CodeToBinary(getOwnerRoomCode);
            string lengthMessage = LengthToBinary(len);

            string message = Code + lengthMessage + newJsonString;
            string response = SendMessage(message);

            string json = GetJsonFromBinary(response);
            var jsonDocument = JsonDocument.Parse(json);
            string root = jsonDocument.RootElement.ToString();
            var myData = JsonSerializer.Deserialize<MyData>(root);

            string[] elements = myData.Room.Split(", ");
            Room room = new Room(elements[0], elements[1], elements[3] == "Public", elements[2], int.Parse(elements[5]), int.Parse(elements[4]), int.Parse(elements[6]), int.Parse(elements[7]), int.Parse(elements[8]));
            return room;
        }

        public static void Close()
        {
            try
            {
                waitingRoomWindow.Visibility = System.Windows.Visibility.Collapsed;
            }
            catch { }
        }

        public static List<UserData> GetAllUsers()
        {
            //Send
            string Sdata = CodeToBinary(getPlayersInRoomCode);
            List<UserData> userList = new List<UserData>();
            string response = SendMessage(Sdata);

            string deCodedString = "";

            for (int i = 0; i < response.Length; i += 8)
            {
                string binaryByte = response.Substring(i, 8);
                byte asciiValue = Convert.ToByte(binaryByte, 2);
                char character = Convert.ToChar(asciiValue);
                if (deCodedString == "")
                {
                    if (character == '{')
                    {
                        deCodedString += character;
                    }
                }
                else
                {
                    deCodedString += character;
                }
            }
            try
            {
                JObject jsonObject = JObject.Parse(deCodedString);
                string playersInRoom = (string)jsonObject["PlayersInRoom"];
                if (playersInRoom == null)
                    return userList;

                string[] users = playersInRoom.Split("\n");

                if (playersInRoom == "")
                    return userList;

                for (int i = 0; i < users.Length; i++)
                {
                    string[] elements = users[i].Split(", ");
                    userList.Add(new UserData(elements[0], elements[2], elements[1], elements[3] == "True"));
                }
            }
            catch { }

            return userList;
        }
        public static List<UserData> GetWaitingUsers()
        {
            string Sdata = CodeToBinary(getWaitingUsersCode);
            List<UserData> userList = new List<UserData>();

            string response = SendMessage(Sdata);

            string deCodedString = "";

            for (int i = 0; i < response.Length; i += 8)
            {
                string binaryByte = response.Substring(i, 8);
                byte asciiValue = Convert.ToByte(binaryByte, 2);
                char character = Convert.ToChar(asciiValue);
                if (deCodedString == "")
                {
                    if (character == '{')
                    {
                        deCodedString += character;
                    }
                }
                else
                {
                    deCodedString += character;
                }
            }
            JObject jsonObject = JObject.Parse(deCodedString);

            try
            {
                string playersInRoom = (string)jsonObject["WaitingPlayers"];
                if (playersInRoom == null)
                    return userList;
                string[] users = playersInRoom.Split("\n");
                for (int i = 0; i < users.Length; i++)
                {
                    string[] elements = users[i].Split(", ");
                    userList.Add(new UserData(elements[0], elements[2], elements[1], false));
                }
            }
            catch { }
            return userList;
        }
        public static string SendAndRecv_Refresh()
        {
            //Send
            string message = CodeToBinary(15);//Code for refresh
            string response = SendMessage(message);
            try
            {
                string code = GetJsonFromBinary(response);
                return code;
            }
            catch
            {
                return "0";
            }        
        }
        public static void SendAndRecv_CloseRoom()
        {
            string message = CodeToBinary(10);//Code for close room
            SendMessage(message);
        }
        public static void SendAndRecv_StartGame(Room room)
        {
            string message = CodeToBinary(14); //Code for start game
            SendMessage(message);
            GameManager.ShowGameWindow(room, MenuManager.username);
        }
        public static void SendAndRecv_LeaveRoom()
        {
            //Send
            string message = CodeToBinary(13);//Code for leave room
            SendMessage(message);
        }

    }
}