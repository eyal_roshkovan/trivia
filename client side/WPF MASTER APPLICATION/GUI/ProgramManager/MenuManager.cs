﻿using GUI.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Windows.Documents;
using System.Runtime.CompilerServices;
using Newtonsoft.Json.Linq;
using System.Reflection.Metadata;

namespace GUI.ProgramManager
{
    public class RoomData
    {
        public string Rooms { get; set; }
        public int Status { get; set; }
        public int id { get; set; }

    }

    public class MenuManager : Manager
    {
        public static string username;
        public static int approved = -1;
        protected const int logOutCode = 3;
        protected const int joinRoomCode = 4;
        protected const int createRoomCode = 5;
        protected const int getRoomsCode = 6;
        protected const int getPlayersInRoomCode = 7;
        protected const int topWinnersCode = 80;
        protected const int topCategoriesCode = 81;
        protected const int personalStatisticsCode = 9;
        protected const int hasApprovedCode = 18;
        protected const int stopWaitingCode = 19;
        public static bool toAskApprove = true;
        public static bool menuActive = true;
        private static JoinRoom joinRoomWindow;
        private static CreateRoom createRoomWindow;
        private static Menu menuWindow;
        private static Statistics statisticsWindow;
        private static PersonalStatistics personalStatisticsWindow;
        private static GeneralStatistics generalStatisticsWindow;
        public static void CloseAllWindows()
        {
            try
            {
                createRoomWindow.Visibility = System.Windows.Visibility.Collapsed;
                joinRoomWindow.Visibility = System.Windows.Visibility.Collapsed;
                menuWindow.Visibility = System.Windows.Visibility.Collapsed;
                personalStatisticsWindow.Visibility = System.Windows.Visibility.Collapsed;
                generalStatisticsWindow.Visibility = System.Windows.Visibility.Collapsed;
                statisticsWindow.Visibility = System.Windows.Visibility.Collapsed;
            }
            catch { }
        }
        public static void DefineAllWindows(string newUsername)
        {
            toAskApprove = true;
            menuActive = true;
            username = newUsername;
            joinRoomWindow = new JoinRoom(username);
            createRoomWindow = new CreateRoom(username);
            menuWindow = new Menu(username);
            statisticsWindow = new Statistics(username);
            personalStatisticsWindow = new PersonalStatistics();
            generalStatisticsWindow = new GeneralStatistics();
            LoginOrSignupManager.HideAll();
        }
        public static void ShowStatistics()
        {
            statisticsWindow.Show();
            try
            {
                createRoomWindow.Visibility = System.Windows.Visibility.Collapsed;
                joinRoomWindow.Visibility = System.Windows.Visibility.Collapsed;
                menuWindow.Visibility = System.Windows.Visibility.Collapsed;
                personalStatisticsWindow.Visibility = System.Windows.Visibility.Collapsed;
                generalStatisticsWindow.Visibility = System.Windows.Visibility.Collapsed;
            }
            catch
            {

            }
        }
        public static void ShowPersonalStatistics()
        {
            personalStatisticsWindow.Show();
            personalStatisticsWindow.Update();
            try
            {
                createRoomWindow.Visibility = System.Windows.Visibility.Collapsed;
                joinRoomWindow.Visibility = System.Windows.Visibility.Collapsed;
                menuWindow.Visibility = System.Windows.Visibility.Collapsed;
                statisticsWindow.Visibility = System.Windows.Visibility.Collapsed;
                generalStatisticsWindow.Visibility = System.Windows.Visibility.Collapsed;
            }
            catch
            {

            }
        }
        public static void ShowGeneralStatistics()
        {
            generalStatisticsWindow.Show();
            generalStatisticsWindow.Update();
            try
            {
                createRoomWindow.Visibility = System.Windows.Visibility.Collapsed;
                joinRoomWindow.Visibility = System.Windows.Visibility.Collapsed;
                menuWindow.Visibility = System.Windows.Visibility.Collapsed;
                statisticsWindow.Visibility = System.Windows.Visibility.Collapsed;
                personalStatisticsWindow.Visibility = System.Windows.Visibility.Collapsed;
            }
            catch
            {

            }
        }
        public static void ShowMenu()
        {
            menuWindow.Show();
            try
            {
                createRoomWindow.Visibility = System.Windows.Visibility.Collapsed;
                joinRoomWindow.Visibility = System.Windows.Visibility.Collapsed;
                statisticsWindow.Visibility = System.Windows.Visibility.Collapsed;
                personalStatisticsWindow.Visibility = System.Windows.Visibility.Collapsed;
                generalStatisticsWindow.Visibility = System.Windows.Visibility.Collapsed;
            }
            catch 
            {

            }
        }
        public static void ShowJoinRoom()
        {
            joinRoomWindow.Show();
            try
            {
                createRoomWindow.Visibility = System.Windows.Visibility.Collapsed;
                menuWindow.Visibility = System.Windows.Visibility.Collapsed;
                statisticsWindow.Visibility = System.Windows.Visibility.Collapsed;
                personalStatisticsWindow.Visibility = System.Windows.Visibility.Collapsed;
                generalStatisticsWindow.Visibility = System.Windows.Visibility.Collapsed;
            }
            catch {

            }
        }
        public static void ShowCreateRoom()
        {
            createRoomWindow.Show();
            try
            {
                joinRoomWindow.Visibility = System.Windows.Visibility.Collapsed;
                menuWindow.Visibility = System.Windows.Visibility.Collapsed;
                statisticsWindow.Visibility = System.Windows.Visibility.Collapsed;
                personalStatisticsWindow.Visibility = System.Windows.Visibility.Collapsed;
                generalStatisticsWindow.Visibility = System.Windows.Visibility.Collapsed;
            }
            catch   
            {

            }
        }

        public static List<Room> RoomDeserializer(string data)
        {
            Room currentRoom = null;
            List<Room> list = new List<Room>();
            string json = GetJsonFromBinary(data);

            try
            {
                JObject jsonObject = JObject.Parse(json);
                string neededData = (string)jsonObject["Room"];
                if(neededData == null)
                {
                    neededData = (string)jsonObject["Rooms"];
                    if(neededData == null)    
                        return list;

                }
                
                string[] rooms = neededData.Split('\n');
                for (int i = 0; i < rooms.Length; i++)
                {
                    string name, owner, category, privacy;
                    int maxPlayers, currentPlayers, id, amountOfQuestions, timeForQuestion;

                    name = rooms[i].Split(", ")[0];

                    owner = rooms[i].Split(", ")[1];

                    category = rooms[i].Split(", ")[2];

                    privacy = rooms[i].Split(", ")[3];

                    maxPlayers = int.Parse(rooms[i].Split(", ")[4]);

                    currentPlayers = int.Parse(rooms[i].Split(", ")[5]);

                    id = int.Parse(rooms[i].Split(", ")[6]);

                    amountOfQuestions = int.Parse(rooms[i].Split(", ")[7]);

                    timeForQuestion = int.Parse(rooms[i].Split(", ")[8]);

                    list.Add(new Room(name, owner, privacy == "True", category, currentPlayers, maxPlayers, id, amountOfQuestions, timeForQuestion));

                }

                return list;

            }
            catch // in case empty
            {
                return list;
            }
        }

        public static List<Tuple<string, int>> GetTopWinners()
        {
            List<Tuple<string, int>> topWinners = new List<Tuple<string, int>>();

            string message = CodeToBinary(topWinnersCode);
            string response = SendMessage(message);
            string json = GetJsonFromBinary(response);

            return topWinners;
        }

        public static List<Tuple<string, int>> GetTopSelectedCategories()
        {
            List<Tuple<string, int>> topCategories = new List<Tuple<string, int>>();

            string message = CodeToBinary(topCategoriesCode);
            string response = SendMessage(message);
            string jsonString = GetJsonFromBinary(response);

            var jsonObject = JObject.Parse(jsonString);

            // Access the list of categories directly
            var categories = jsonObject["Categories"];

            // Convert to List<Tuple<string, int>>
            topCategories = categories?.ToObject<List<Tuple<string, int>>>() ?? new List<Tuple<string, int>>();


            return topCategories;
        }

        public static List<string> GetPersonalStats()
        {
            List<string> personalStats = new List<string>();
            string message = CodeToBinary(personalStatisticsCode);
            string response = SendMessage(message);
            string jsonString = GetJsonFromBinary(response);

            var jsonObject = JObject.Parse(jsonString);

            // Access the list of categories directly
            var stats = jsonObject["Stats"];

            // Convert to List<Tuple<string, int>>
            personalStats = stats?.ToObject<List<string>>() ?? new List<string>();


            return personalStats;
        }

        public static void GetRoomResponseForJoining()
        {
            while (toAskApprove)
            {
                var jsonFormat = new
                { };

                string jsonString = JsonSerializer.Serialize(jsonFormat);
                string newJsonString = "";
                foreach (char c in jsonString)
                {
                    newJsonString += StringToBinary(c.ToString());
                }
                int len = byteSize * jsonString.Length;
                string code = CodeToBinary(hasApprovedCode);
                string lengthMessage = LengthToBinary(len);

                string message = code + lengthMessage + newJsonString;
                string response = SendMessage(message);

                int result = OneIntResponseDeserializer(response);

                if(result != -1)
                {
                    toAskApprove = false;
                    approved = result;
                    return;
                }
            }
        }

        public static void StopAskForJoiningRoom()
        {
            var jsonFormat = new
            { };

            string jsonString = JsonSerializer.Serialize(jsonFormat);
            string newJsonString = "";
            foreach (char c in jsonString)
            {
                newJsonString += StringToBinary(c.ToString());
            }
            int len = byteSize * jsonString.Length;
            string Code = CodeToBinary(stopWaitingCode);
            string lengthMessage = LengthToBinary(len);

            string message = Code + lengthMessage + newJsonString;
            SendMessage(message);
            toAskApprove = false;
        }

        public static bool TryJoinRoom(Room room)
        {
            var jsonFormat = new
            {
                roomID = room.id
            };

            string jsonString = JsonSerializer.Serialize(jsonFormat);
            string newJsonString = "";
            foreach (char c in jsonString)
            {
                newJsonString += StringToBinary(c.ToString());
            }
            int len = byteSize * jsonString.Length;
            string Code = CodeToBinary(joinRoomCode);
            string lengthMessage = LengthToBinary(len);

            string message = Code + lengthMessage + newJsonString;
            string response = SendMessage(message);

            bool isOK = OneIntResponseDeserializer(response) == 1;

            return isOK;
        }

        public static bool TryCreateRoom(string roomName, string category, int maxUsers, int questionCount, int answerTimeout, string privacy)
        {
            var jsonFormat = new
            {
                roomName, category, maxUsers, questionCount, answerTimeout, privacy
            };
            string jsonString = JsonSerializer.Serialize(jsonFormat);
            string newJsonString = "";
            foreach (char c in jsonString)
            {
                newJsonString += StringToBinary(c.ToString());
            }
            int len = byteSize * jsonString.Length;
            string code = CodeToBinary(createRoomCode);
            string lengthMessage = LengthToBinary(len);

            string message = code + lengthMessage + newJsonString;
            string response = SendMessage(message);

            bool isOK = OneIntResponseDeserializer(response) == 1;

            return isOK;
        }
        public static void Logout()
        {
            menuActive = false;
            toAskApprove = false;
            SendMessage(CodeToBinary(logOutCode));
            CloseAllWindows();
            LoginOrSignupManager.ShowLogin();
        }
        public static List<Room> GetRooms()
        {
            var jsonFormat = new { };
            string jsonString = JsonSerializer.Serialize(jsonFormat);
            string newJsonString = "";
            foreach (char c in jsonString)
            {
                newJsonString += StringToBinary(c.ToString());
            }
            int len = byteSize * jsonString.Length;
            string Code = CodeToBinary(getRoomsCode);
            string lengthMessage = LengthToBinary(len);

            string message = Code + lengthMessage + newJsonString;
            string response = SendMessage(message);

            return RoomDeserializer(response);
        }
    }
}
