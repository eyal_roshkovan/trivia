﻿using GUI.ProgramManager;
using GUI.Views;
using System;
using System.Windows;

namespace GUI
{
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            LoginOrSignupManager.Run(8826);
        }
    }
}
